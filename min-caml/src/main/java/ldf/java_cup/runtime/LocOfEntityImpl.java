package ldf.java_cup.runtime;

/*
    The Symbol and Comment class uses this class to get information about their position in the file.
 */
@SuppressWarnings("unused")

public class LocOfEntityImpl implements LocOfEntity {

    protected int lLeft, lRight, ColRight, ColLeft, offsetR, offsetL = -1;

    @Override
    public void setLeftPosition(LocOfEntity e) {
        setLeftPosition(e.getLLeft(), e.getColLeft(), e.getOffsetLeft());
    }

    public void setLeftPosition(int l, int col, int offset) {
        lLeft = l;
        ColLeft = col;
        offsetL = offset;
    }

    @Override
    public void setRightPosition(LocOfEntity r) {
        setRightPosition(r.getLRight(), r.getColRight(), r.getOffsetRight());
    }

    public void setRightPosition(int line, int col, int offset) {
        lRight = line;
        ColRight = col;
        offsetR = offset;
    }

    public final int getLLeft() {
        return lLeft;
    }

    public final int getLRight() {
        return lRight;
    }

    public final int getColLeft() {
        return ColLeft;
    }

    public final int getColRight() {
        return ColRight;
    }

    public final int getOffsetLeft() {
        return offsetL;
    }

    public final int getOffsetRight() {
        return offsetR;
    }

}