package ldf.java_cup.runtime;

/**
 * Defines the Symbol class, which is used to represent all terminals
 * and nonterminals while parsing. The lexer should pass CUP Symbols
 * and CUP returns a Symbol.
 *
 * @version last updated: 7/3/96
 * @author Frank Flannery
 * 
 */

@SuppressWarnings("unused")
public class Symbol extends LocOfEntityImpl {

    protected String symbolName; // symbol name used for debugging puposes

    protected int symbolCode; // code for the symbol (provided by CUP)

    public Object value;

    /**
     * Symbol object could be a Comment object.
     * If used, this field should be populated by the lexer create new comment using
     * the token factory that points
     * to the comment (if any) that's located right before this token.
     * If the comment doesn't exist, this is set to null.
     * If there are many comments, this should point to the one right before
     * similar to a linked list.
     */
    protected Comment previousC;

    /**
     * The parse state to be recorded on the parse stack with this symbol.
     * This field is for the convenience of the parser and shouldn't be
     * modified except by the parser.
     */
    protected int parse_state;

    /**
     * This allows us to catch some errors caused by scanners recycling
     * symbols. For the use of the parser only. [CSA, 23-Jul-1999]
     */
    boolean used_by_parser;

    public Symbol() {
    }

    public Symbol(String sName, int sCode) {
        symbolName = sName;
        symbolCode = sCode;
    }

    public Symbol(String sName, int sCode, int Status) {
        this(sName, sCode);
        parse_state = Status;
    }

    // explain
    public void setLeftRightSymbols(Symbol l, Symbol r) {
        setRightPosition(r.lRight, r.ColRight, r.offsetR);
        setLeftPosition(l.lLeft, l.ColLeft, l.offsetL);
        previousC = l.previousC;
    }

    public String getSymbolName() {
        return symbolName;
    }

    public int getSymbolCode() {
        return symbolCode;
    }

    /**
     * Sets the previousC to the comment before this symbol.
     * Similarly the comment will also point to its previous...
     */
    public void setpreviousC(Comment previousC) {
        this.previousC = previousC;
    }

    public boolean hasComments() {
        return previousC != null;
    }

    /**
     * Enumeration of the comments before this symbol (in reverse order)
     */
    public CommentEnum getCommentsReverse() {
        return new CommentEnum(previousC);
    }

    @Override
    public String toString() {
        return symbolName + " (" + lLeft + ":" + ColLeft + ")";
    }

}