package ldf.java_cup.runtime;
// To get the prveious comment using linkedlist
public class Comment extends LocOfEntityImpl {

    protected Comment previousC;

    protected Comment() {}

    public Comment(Comment previousC) {
        this.previousC = previousC;
    }

    public Comment getpreviousC() {
        return previousC;
    }

}
