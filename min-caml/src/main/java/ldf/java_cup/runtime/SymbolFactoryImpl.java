package ldf.java_cup.runtime;

public class SymbolFactoryImpl implements SymbolFactory {

    /*
     * This method directly instantiates our Symbol objects.
     * We should override this method in case sub-classes are created.
     */
    protected Symbol newSymbol(String symName, int symCode, int parse_status) {
        return new Symbol(symName, symCode, parse_status);
    }

    /*
     * This Function is used by CUP to create the start symbol ( head in our Sumbol
     * class).
     * The status of this symbol should be set explicitly in order for the parser to
     * work.
     */
    @Override
    public Symbol startSymbol(String symName, int symCode, int parse_status) {
        return newSymbol(symName, symCode, parse_status);
    }

    @Override
    public Symbol newSymbol(String Name, int id) {
        return newSymbol(Name, id, 0);
    }

    @Override
    public Symbol newSymbol(String Name, int id, Symbol left, Symbol right) {
        Symbol s = newSymbol(Name, id);
        s.setLeftRightSymbols(left, right);
        return s;
    }

    @Override
    public Symbol newSymbol(String Name, int id, Object val) {
        Symbol symb = newSymbol(Name, id);
        symb.value = val;
        return symb;
    }

    @Override
    public Symbol newSymbol(String Name, int id, Symbol left, Symbol right, Object val) {
        Symbol s = newSymbol(Name, id, left, right);
        s.value = val;
        return s;
    }

    @Override
    public Symbol newEmptySymbol(String Name, int id, Symbol prevSym) {
        Symbol s = newSymbol(Name, id);
        s.setLeftPosition(prevSym.lRight, prevSym.lRight, prevSym.offsetR);
        s.setRightPosition(prevSym.lRight, prevSym.ColRight, prevSym.offsetR);
        return s;
    }

    @Override
    public Symbol newEmptySymbol(String Name, int id, Symbol prevSym, Object val) {
        Symbol s = newEmptySymbol(Name, id, prevSym);
        s.value = val;
        return s;
    }

}