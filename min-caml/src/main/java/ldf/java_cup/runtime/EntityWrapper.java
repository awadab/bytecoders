package ldf.java_cup.runtime;

/*
  We created this class to facilitate the access to the wrapped object information.
  The warped object is a LocOfEntity.
 */
public abstract class EntityWrapper implements LocOfEntity {

    protected abstract LocOfEntity getLocOfEntity();

    @Override
    public final void setLeftPosition(LocOfEntity left) {
        getLocOfEntity().setLeftPosition(left);
    }

    @Override
    public final void setLeftPosition(int line, int column, int offset) {
        getLocOfEntity().setLeftPosition(line, column, offset);
    }

    @Override
    public final void setRightPosition(LocOfEntity right) {
        getLocOfEntity().setRightPosition(right);
    }

    @Override
    public final void setRightPosition(int line, int column, int offset) {
        getLocOfEntity().setRightPosition(line, column, offset);
    }

    @Override
    public final int getLLeft() {
        return getLocOfEntity().getLLeft();
    }

    @Override
    public final int getLRight() {
        return getLocOfEntity().getLRight();
    }

    @Override
    public final int getColLeft() {
        return getLocOfEntity().getColLeft();
    }

    @Override
    public final int getColRight() {
        return getLocOfEntity().getColRight();
    }

    @Override
    public int getOffsetLeft() {
        return getLocOfEntity().getOffsetLeft();
    }

    @Override
    public int getOffsetRight() {
        return getLocOfEntity().getOffsetRight();
    }
}