package ldf.java_cup.runtime;

import java.util.Enumeration;
import java.util.NoSuchElementException;

public class CommentEnum implements Enumeration<Comment> {

    private Comment curr;

    public CommentEnum(Comment head) {
        curr = head; // starts from the first till the position of the last comment
    }

    @Override
    public boolean hasMoreElements() { // Tests if this enumeration contains more elements.
        // Returns: true if and only if this enumeration object contains at least one
        // more element to provide; false otherwise
        return curr != null;
    }

    @Override
    public Comment nextElement() { // Returns the next element of this enumeration
        Comment next = curr;
        if (next == null) {
            throw new NoSuchElementException();
        }
        curr = next.previousC;
        return next;
    }
}
