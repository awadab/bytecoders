package ldf.java_cup.runtime;

/**
 * This interface is used so that we can save the last object returned before
 * a new one is detected. This is used so that the lexer could get
 * the line and column of the last object detected.
 */
public interface TokenFactory extends SymbolFactory {

        /*
         * These methods are used to create Symbols where we know the LPosition
         * and set the Rposition of the last object if it is not set.
         */
        Symbol newToken(String symName, int symCode, int lineL, int columnL, int offsetL);      
        Symbol newToken(String symName, int symCode, int lineL, int columnL, int offsetL, int lineR, int columnR,
                        int offsetR);   

        Symbol newEOF(String symName, int symCode, int lineL, int columnL, int offsetL); // special for handling end of file.      
        
        /**
         * This function will be used by the Lexer to store Comment just found 
         * and then the lexer will link it to the next one.
         */
        Comment newComment(int lineL, int columnL, int offsetL);        
        
        /**
         * Mark the end of the token when a whitespace is detected.
         */
        void signalWhitespace(int lineL, int columnL, int offsetL);

}
