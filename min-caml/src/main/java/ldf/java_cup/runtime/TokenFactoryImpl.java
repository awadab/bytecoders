package ldf.java_cup.runtime;


public class TokenFactoryImpl extends SymbolFactoryImpl implements TokenFactory {

   protected Comment lastComment;
   protected Symbol lastSymbol;
   protected boolean lastRightPosWasSet;
   @Override
   public Symbol newToken(String symName, int symCode, int lineL, int columnL, int offsetL) {
           markRightPosition(lineL, columnL, offsetL);
           Symbol s = newSymbol(symName, symCode);
           s.setLeftPosition(lineL, columnL, offsetL);
           s.previousC = lastComment;
           lastSymbol = s;
           lastComment = null;
           lastRightPosWasSet = false;
           return s;
   }
   @Override
   public Symbol newToken(String symName, int symCode, int lineL, int columnL, int offsetL, int lineR, int columnR,
                   int offsetR) {
           newToken(symName, symCode, lineL, columnL, offsetL);
           markRightPosition(lineR, columnR, offsetR);
           return lastSymbol;
   }
   @Override
   public Symbol newEOF(String symName, int symCode, int lineL, int columnL, int offsetL) {
           markRightPosition(lineL, columnL, offsetL);
           Symbol t = newToken(symName, symCode, lineL, columnL, offsetL);
           t.setRightPosition(lineL, columnL, offsetL);
           lastRightPosWasSet = true;
           lastSymbol = null;
           return t;
   }
   @Override
   public Comment newComment(int lineL, int columnL, int offsetL) {
           markRightPosition(lineL, columnL, offsetL);
           Comment c = new Comment(lastComment);
           c.setLeftPosition(lineL, columnL, offsetL);
           lastComment = c;
           return c;
   }
   @Override
   public void signalWhitespace(int lineL, int columnL, int offsetL) {
           markRightPosition(lineL, columnL, offsetL);
   }
   /* 
    * Mark the end position of the previously returned object.
    */
   protected void markRightPosition(int line, int column, int offset) {
           if (lastRightPosWasSet)
                   return;
           LocOfEntityImpl last = (lastComment != null) ? lastComment : lastSymbol;
           if (last != null) {
                   last.setRightPosition(line, column, offset);
           }
           lastRightPosWasSet = true;
   }

}
