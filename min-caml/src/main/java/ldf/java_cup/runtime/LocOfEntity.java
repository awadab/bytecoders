package ldf.java_cup.runtime;

/* 
It is an entity that knows its position in the file. The position information could be stored in
the object itself (See LocOfEntityImpl) or if the object is a (LocOfEntityWrapper), the position will
be in the object contained by the wrapper.
*/

public interface LocOfEntity {

    void setRightPosition(LocOfEntity right);

    void setRightPosition(int l, int col, int offset);

    void setLeftPosition(LocOfEntity left);

    void setLeftPosition(int l, int col, int offset);

    int getLRight();

    int getColRight();

    int getOffsetRight();

    int getLLeft();

    int getColLeft();

    int getOffsetLeft();

}
