package min_camel.visit;

import min_camel.knorm.*;
import min_camel.type.Type;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;
import min_camel.helpers.SymbolTable;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@ParametersAreNonnullByDefault
public final class KnormFreeVars extends KnormDummyVisitor {

    private List<SymRef> free = new LinkedList<>();
    private SymbolTable<Type> symbolTable;

    private KnormFreeVars() {
    }

    public static List<SymRef> compute(KnormNode root, SymbolTable<Type> symTable) {
        KnormFreeVars fv = new KnormFreeVars();
        fv.symbolTable = symTable;
        root.accept(fv);
        return fv.free;
    }

    public static List<SymRef> compute(KnormNode root) {
        KnormFreeVars fv = new KnormFreeVars();
        fv.symbolTable = new SymbolTable<>();
        root.accept(fv);
        return fv.free;
    }

    public void visit(KnormLet k) {
        symbolTable.push();
        {
            k.init.accept(this);
            symbolTable.put(k.Id.id, k.Id.type);
            k.ret.accept(this);
        }
        symbolTable.pop();
    }

    public void visit(KnormLetRec k) {
        symbolTable.push();
        {
            freeCheck(k.FuncDef.FreeVars);
            symbolTable.put(k.FuncDef.FuncName.id, k.FuncDef.FuncName.type);
            for (SymDef arg : k.FuncDef.FuncArgs) {
                symbolTable.put(arg.id, arg.type);
            }
            k.ret.accept(this);
        }
        symbolTable.pop();
    }

    public void visit(KnormLetTuple k) {
        symbolTable.push();
        {
            freeCheck(k.init);
            for (SymDef def : k.Ids) {
                symbolTable.put(def.id, def.type);
            }
            k.ret.accept(this);
        }
        symbolTable.pop();
    }

    public void visit(KnormCallFunc k) {
        freeCheck(k.FuncObject);
        freeCheck(k.BoundedArgs);
    }

    public void visit(KnormDirect k) {
        freeCheck(k.args);
    }

    public void visit(KnormClosureMake k) {
        symbolTable.push();
        {
            freeCheck(k.FreeArgs);
            symbolTable.put(k.target);
            k.ret.accept(this);
        }
        symbolTable.pop();
    }

    private void freeCheck(SymRef ref) {
        if (symbolTable.get(ref.id) != null) return;
        free.add(ref);
    }

    private void freeCheck(Collection<SymRef> refs) {
        for (SymRef ref : refs) {
            freeCheck(ref);
        }
    }

    public void visit(KnormVar k) {
        freeCheck(k.Variable);
    }

    public void visit(KnormTuple k) {
        freeCheck(k.Elements);
    }

    public void visit(KnormNeg k) {
        freeCheck(k.opr);
    }

    public void visit(KnormAdd k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
    }

    public void visit(KnormSub k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
    }

    public void visit(KnormFloatNeg k) {
        freeCheck(k.opr);
    }

    public void visit(KnormFloatAdd k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
    }

    public void visit(KnormFloatSub k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
    }

    public void visit(KnormFloatMul k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
    }

    public void visit(KnormFloatDiv k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
    }

    public void visit(KnormGet k) {
        freeCheck(k.array);
        freeCheck(k.index);
    }

    public void visit(KnormPut k) {
        freeCheck(k.arr);
        freeCheck(k.index);
        freeCheck(k.value);
    }

    public void visit(KnormIfEqual k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
        super.visit(k);
    }

    public void visit(KnormIfLess k) {
        freeCheck(k.opr1);
        freeCheck(k.opr2);
        super.visit(k);
    }

    public void visit(KnormApplication k) {
        freeCheck(k.symbRef);
        freeCheck(k.args);
    }


}
