package min_camel.visit;

import min_camel.ast.*;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public final class FunctionSize extends DummyVisitor {

    private int numOfOpreations;

    private FunctionSize() {
    }

    public static int compute(AstExp astNode) {
        FunctionSize fs = new FunctionSize();
        astNode.accept(fs);
        return fs.numOfOpreations;
    }

    public void visit(AstSub e) {
        numOfOpreations ++;
        e.e1.accept(this);
        e.e2.accept(this);
    }
    public void visit(AstFSub e) {
        numOfOpreations ++;
        e.e1.accept(this);
        e.e2.accept(this);
    }

    public void visit(AstAdd e) {
        numOfOpreations ++;
        e.e1.accept(this);
        e.e2.accept(this);
    }

    public void visit(AstFMul e) {
        numOfOpreations ++;
        e.e1.accept(this);
        e.e2.accept(this);
    }

    public void visit(AstFDiv e) {
        numOfOpreations ++;
        e.e1.accept(this);
        e.e2.accept(this);
    }

    public void visit(AstEq e) {
        numOfOpreations ++;
        e.e1.accept(this);
        e.e2.accept(this);
    }

    public void visit(AstLet e) {
        numOfOpreations ++;
        e.ret.accept(this);
    }

}
