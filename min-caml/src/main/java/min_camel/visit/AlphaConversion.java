package min_camel.visit;

import min_camel.ast.*;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;
import min_camel.helpers.SymbolTable;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.List;

/*
 * Performs alpha conversion that ensures that no symbol is bounded more than once
 * by nested Let expressions
 */
@ParametersAreNonnullByDefault
public final class AlphaConversion extends TransformHelper {

    // To help generate new unique variable names.
    private int lastId = 0;

    // Map for renamed variables
    private SymbolTable<String> reMap = new SymbolTable<>();

    private AlphaConversion() {
    }

    // Rename IDs.
    @Nonnull
    private String newId(String name) {
        return name + "_" + (++lastId);
    }

    /**
     * Transform the input AST to a new one corresponding to the changes 
     * of the alpha conversion
     */
    @Nonnull
    public static AstExp transform(AstExp astNode) {
        return astNode.accept(new AlphaConversion());
    }

    /**
     * After a Let expression we rename the identifier such that it is unique
     * in the current environment.
     */
    @Nonnull
    public AstExp visit(AstLet e) {
        // Rename the identifier available in the Let expression
        SymDef old_id = e.decl;
        SymDef new_id = old_id.RenameSymbol(newId(old_id.id));

        // Recursively perform any changes in the body of the Let expression
        AstExp new_e1 = e.initializer.accept(this);
        AstExp new_e2;

        reMap.push();
        {
            reMap.put(old_id.id, new_id.id);
            new_e2 = e.ret.accept(this);
        }
        reMap.pop();

        return new AstLet(new_id, new_e1, new_e2);
    }

    /**
     * When a variable is used, we check if it was renamed or not and
     * we return the name.
     */
    @Nonnull
    public AstExp visit(SymRef e) {
        // Get the symbol referenced in the environment.
        String Old_Id = e.id;
        String New_Id = reMap.get(Old_Id);

        // If we couldn't get the symbol, return the old name.
        if (New_Id == null || New_Id.equals(Old_Id)) return e;

        // If the name was changed, return the new AST node.
        return new SymRef(New_Id);
    }

    @Nonnull
    public AstExp visit(AstLetRec e) {
        AstFunDef Old_FuncDef = e.fd;
        AstFunDef New_FuncDef = (AstFunDef) Old_FuncDef.accept(this);
        AstExp Old_Expr = e.ret;
        AstExp New_Expr;

        reMap.push();
        {
            reMap.put(Old_FuncDef.decl.id, New_FuncDef.decl.id);
            New_Expr = Old_Expr.accept(this);
        }
        reMap.pop();

        if (Old_FuncDef == New_FuncDef && Old_Expr == New_Expr) return e;

        return new AstLetRec(New_FuncDef, New_Expr);
    }


    @Nonnull
    public AstExp visit(AstFunDef e) {
        SymDef Old_Id = e.decl;
        SymDef New_Id = Old_Id.RenameSymbol(newId(Old_Id.id));

        AstExp Old_Expr = e.body;
        AstExp New_Expr;

        List<SymDef> Old_Args = e.args;
        List<SymDef> New_Args = new ArrayList<>();


        reMap.push();
        {
            reMap.put(Old_Id.id, New_Id.id);
            for (SymDef Oldarg : Old_Args) {
                SymDef NewArg = Oldarg.RenameSymbol(newId(Oldarg.id));
                New_Args.add(NewArg);
                reMap.put(Oldarg.id, NewArg.id);
            }
            New_Expr = Old_Expr.accept(this);
        }
        reMap.pop();

        if (New_Id == Old_Id && New_Args.equals(Old_Args) && New_Expr == Old_Expr) return e;
        return new AstFunDef(New_Id, New_Args, New_Expr);
    }


    @Nonnull
    public AstExp visit(AstApp e) {
        AstExp Old_Expr = e.e;
        AstExp New_Expr;

        List<AstExp> Old_Expr_list = e.es;
        List<AstExp> New_Expr_list = new ArrayList<>();

        reMap.push();
        {
            for (AstExp OldE : Old_Expr_list) {
                New_Expr_list.add(OldE.accept(this));
            }
            New_Expr = Old_Expr.accept(this);
        }
        reMap.pop();

        if (New_Expr == Old_Expr && New_Expr_list.equals(Old_Expr_list)) return e;
        return new AstApp(New_Expr, New_Expr_list);
    }

}
