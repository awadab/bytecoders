package min_camel.visit;

import min_camel.ast.*;
import min_camel.helpers.SymRef;
import min_camel.helpers.Visitor2;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;


public abstract class TransformHelper2<T> implements Visitor2<AstExp, T> {


    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstErr e) {
        return e;
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstUnit e) {
        return e;
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull SymRef e) {
        return e;
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstInt e) {
        return e;
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstFloat e) {
        return e;
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstBool e) {
        return e;
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstNot e) {
        AstExp Old_Expr = e.e; 
        AstExp New_Expr = Old_Expr.accept(this, ctx); 
        if (New_Expr == Old_Expr) { 
            return e; 
        }
        return new AstNot(New_Expr); 
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstNeg e) {
        AstExp Old_Expr = e.e;
        AstExp New_Expr = Old_Expr.accept(this, ctx);
        if (New_Expr == Old_Expr) return e;
        return new AstNeg(New_Expr);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstFNeg e) {
        AstExp Old_Expr = e.e;
        AstExp New_Expr = Old_Expr.accept(this, ctx);
        if (New_Expr == Old_Expr) return e;
        return new AstFNeg(New_Expr);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstAdd e) {
        AstExp Old_Expr1 = e.e1; 
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx); 
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e; 
        return new AstAdd(New_Expr1, New_Expr2); 
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstFAdd e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFAdd(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstSub e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstSub(New_Expr1, New_Expr2);
    }


    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstFSub e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFSub(New_Expr1, New_Expr2);
    }
    
    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstEq e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstEq(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstFMul e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFMul(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstFDiv e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFDiv(New_Expr1, New_Expr2);
    }


    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstLE e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstLE(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstLet e) {
        AstExp Old_Expr1 = e.initializer;
        AstExp Old_Expr2 = e.ret;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == e.initializer && New_Expr2 == e.ret) return e;
        return new AstLet(e.decl, New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstLetTuple e) {
        AstExp Old_Expr1 = e.initializer;
        AstExp Old_Expr2 = e.ret;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == e.initializer && New_Expr2 == e.ret) return e;
        return new AstLetTuple(e.ids, New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstArray e) {
        AstExp Old_Expr1 = e.size;
        AstExp Old_Expr2 = e.initializer;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == e.size && New_Expr2 == e.initializer) return e;
        return new AstArray(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstGet e) {
        AstExp Old_Expr1 = e.array;
        AstExp Old_Expr2 = e.index;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        if (New_Expr1 == e.array && New_Expr2 == e.index) return e;
        return new AstGet(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstIf e) {
        AstExp Old_Expr1 = e.eCond;
        AstExp Old_Expr2 = e.eThen;
        AstExp Old_Expr3 = e.eElse;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        AstExp New_Expr3 = Old_Expr3.accept(this, ctx);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2 && New_Expr3 == Old_Expr3) return e;
        return new AstIf(New_Expr1, New_Expr2, New_Expr3);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstPut e) {
        AstExp Old_Expr1 = e.array;
        AstExp Old_Expr2 = e.index;
        AstExp Old_Expr3 = e.value;
        AstExp New_Expr1 = Old_Expr1.accept(this, ctx);
        AstExp New_Expr2 = Old_Expr2.accept(this, ctx);
        AstExp New_Expr3 = Old_Expr3.accept(this, ctx);
        if (New_Expr1 == e.array && New_Expr2 == e.index && New_Expr3 == e.value) return e;
        return new AstPut(New_Expr1, New_Expr2, New_Expr3);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstApp e) {
        AstExp Old_Expr = e.e;
        AstExp New_Expr = Old_Expr.accept(this, ctx);
        List<AstExp> new_args = new ArrayList<>(e.es.size());
        for (AstExp old_arg : e.es) {
            new_args.add(old_arg.accept(this, ctx));
        }
        if (New_Expr.equals(e.e) && new_args.equals(e.es)) return e;
        return new AstApp(New_Expr, new_args);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstTuple e) {
        List<AstExp> new_items = new ArrayList<>(e.es.size());
        for (AstExp old_item : e.es) {
            new_items.add(old_item.accept(this, ctx));
        }
        if (new_items.equals(e.es)) return e;
        return new AstTuple(new_items);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstFunDef e) {
        AstExp Old_Expr = e.body;
        AstExp New_Expr = Old_Expr.accept(this, ctx);
        if (New_Expr == Old_Expr) return e;
        return new AstFunDef(e.decl, e.args, New_Expr);
    }

    @Nonnull
    public AstExp visit(T ctx, @Nonnull AstLetRec e) {
        AstExp Old_Expr = e.ret;
        AstExp New_Expr = Old_Expr.accept(this, ctx);
        AstFunDef old_fd = e.fd;
        AstFunDef new_fd = (AstFunDef) old_fd.accept(this, ctx);
        if (new_fd == e.fd && New_Expr == e.ret) return e;
        return new AstLetRec(new_fd, New_Expr);
    }

}
