package min_camel.visit;

import min_camel.knorm.*;
import min_camel.helpers.KnormVisitor;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public abstract class KnormDummyVisitor implements KnormVisitor {
    public void visit(KnormUnit k) {
        // do nothing
    }

    public void visit(KnormInt k) {
        // do nothing
    }

    public void visit(KnormFloat k) {
        // do nothing
    }

    public void visit(KnormVar k) {
        // do nothing
    }

    public void visit(KnormTuple k) {
        // do nothing
    }

    public void visit(KnormArr k) {
        // do nothing
    }

    public void visit(KnormNeg k) {
        // do nothing
    }

    public void visit(KnormAdd k) {
        // do nothing
    }

    public void visit(KnormSub k) {
        // do nothing
    }

    public void visit(KnormFloatNeg k) {
        // do nothing
    }

    public void visit(KnormFloatAdd k) {
        // do nothing
    }

    public void visit(KnormFloatDiv k) {
        // do nothing
    }

    public void visit(KnormFloatMul k) {
        // do nothing
    }

    public void visit(KnormFloatSub k) {
        // do nothing
    }

    public void visit(KnormGet k) {
        // do nothing
    }

    public void visit(KnormPut k) {
        // do nothing
    }

    public void visit(KnormLet k) {
        k.init.accept(this);
        k.ret.accept(this);
    }

    public void visit(KnormLetRec k) {
        k.FuncDef.FuncBody.accept(this);
        k.ret.accept(this);
    }

    public void visit(KnormLetTuple k) {
        k.ret.accept(this);
    }

    public void visit(KnormIfEqual k) {
        k.kThen.accept(this);
        k.kElse.accept(this);
    }

    public void visit(KnormIfLess k) {
        k.kThen.accept(this);
        k.kElse.accept(this);
    }

    public void visit(KnormApplication k) {
        // do nothing
    }

    public void visit(KnormCallFunc k) {
        // do nothing
    }

    public void visit(KnormDirect k) {
        // do nothing
    }

    public void visit(KnormClosureMake k) {
        // do nothing
    }
}
