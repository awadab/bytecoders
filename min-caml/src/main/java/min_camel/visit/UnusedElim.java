package min_camel.visit;

import min_camel.ast.AstExp;
import min_camel.ast.AstLet;
import min_camel.ast.AstLetRec;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Set;

@ParametersAreNonnullByDefault
public final class UnusedElim extends TransformHelper {

    private Set<String> unused;

    private UnusedElim(AstExp e) {
        unused = UnusedVar.compute(e);
    }

    // In this function we do the transformation
    public static AstExp Execute(AstExp astNode) {
        return astNode.accept(new UnusedElim(astNode));
    }

    @Nonnull
    public AstExp visit(AstLet e) {
        if (unused.contains(e.decl.id)) {
            return e.ret.accept(this);
        }
        return super.visit(e);
    }

    @Nonnull
    public AstExp visit(AstLetRec e) {
        if (unused.contains(e.fd.decl.id)) {
            return e.ret.accept(this);
        }
        return super.visit(e);
    }

}
