package min_camel.visit;

import min_camel.ast.*;
import min_camel.helpers.SymRef;
import min_camel.helpers.Visitor1;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.List;

/**
 * This abstract class is used transfomation on ASTs while keeping it immutable.
 * It is a base-class for programs that take ASTs as input and return the
 * transformed AST. in the sub-classes we override the methods depending
 * on the type.
 * The transformation is done in 2 steps.First the top-down approach which
 * recursively visit each node of the AST. Second is the bottom-up approach 
 * which return from each recursive call while checking if the children are modified
 * or not. If no changes were made we return the same old node, otherwise we 
 * return a node of the same type with the changed children values.
 * If few child nodes are changed, since the AST is immutable, the unchanged nodes
 * will be reused to recreate the output AST.
 */
@ParametersAreNonnullByDefault
public abstract class TransformHelper implements Visitor1<AstExp> {

    // Arity = 0. No children to verify.

    @Nonnull
    public AstExp visit(AstErr e) {
        return e;
    }

    @Nonnull
    public AstExp visit(SymRef e) {
        return e;
    }

    @Nonnull
    public AstExp visit(AstUnit e) {
        return e;
    }

    @Nonnull
    public AstExp visit(AstInt e) {
        return e;
    }

    @Nonnull
    public AstExp visit(AstFloat e) {
        return e;
    }

    @Nonnull
    public AstExp visit(AstBool e) {
        return e;
    }


    /**
     * Arity = 1. One child to verify.
     * In the following methods, we save the Old nodes and after applying
     * the transformation we get the New nodes. We then check if there are unchanged
     * nodes after the transformation, if so mentioned above we return the old
     * nodes. When changes are made we return the new node with the initialized values.
     */

    @Nonnull
    public AstExp visit(AstNot e) {
        AstExp Old_Expr = e.e; // save old nodes
        AstExp New_Expr = Old_Expr.accept(this); // create new nodes
        if (New_Expr == Old_Expr) { // check changes
            return e; // return unchanged node
        }
        return new AstNot(New_Expr); // return new node
    }

    @Nonnull
    public AstExp visit(AstNeg e) {
        AstExp Old_Expr = e.e;
        AstExp New_Expr = Old_Expr.accept(this);
        if (New_Expr == Old_Expr) return e;
        return new AstNeg(New_Expr);
    }

    @Nonnull
    public AstExp visit(AstFNeg e) {
        AstExp Old_Expr = e.e;
        AstExp New_Expr = Old_Expr.accept(this);
        if (New_Expr == Old_Expr) return e;
        return new AstFNeg(New_Expr);
    }

    /*
        Arity = 2. Two children to check.
     */

    @Nonnull
    public AstExp visit(AstAdd e) {
        AstExp Old_Expr1 = e.e1; // save old nodes
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this); // create new nodes
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e; // check changes and return unchanged nodes
        return new AstAdd(New_Expr1, New_Expr2); // return new nodes
    }

    @Nonnull
    public AstExp visit(AstSub e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstSub(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstFAdd e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFAdd(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstFSub e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFSub(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstFMul e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFMul(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstFDiv e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstFDiv(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstEq e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstEq(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstLE e) {
        AstExp Old_Expr1 = e.e1;
        AstExp Old_Expr2 = e.e2;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2) return e;
        return new AstLE(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstLet e) {
        AstExp Old_Expr1 = e.initializer;
        AstExp Old_Expr2 = e.ret;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == e.initializer && New_Expr2 == e.ret) return e;
        return new AstLet(e.decl, New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstLetTuple e) {
        AstExp Old_Expr1 = e.initializer;
        AstExp Old_Expr2 = e.ret;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == e.initializer && New_Expr2 == e.ret) return e;
        return new AstLetTuple(e.ids, New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstArray e) {
        AstExp Old_Expr1 = e.size;
        AstExp Old_Expr2 = e.initializer;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == e.size && New_Expr2 == e.initializer) return e;
        return new AstArray(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstGet e) {
        AstExp Old_Expr1 = e.array;
        AstExp Old_Expr2 = e.index;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        if (New_Expr1 == e.array && New_Expr2 == e.index) return e;
        return new AstGet(New_Expr1, New_Expr2);
    }

    /*
        Arity = 3. Three children to check.
     */

    @Nonnull
    public AstExp visit(AstIf e) {
        AstExp Old_Expr1 = e.eCond;
        AstExp Old_Expr2 = e.eThen;
        AstExp Old_Expr3 = e.eElse;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        AstExp New_Expr3 = Old_Expr3.accept(this);
        if (New_Expr1 == Old_Expr1 && New_Expr2 == Old_Expr2 && New_Expr3 == Old_Expr3) return e;
        return new AstIf(New_Expr1, New_Expr2, New_Expr3);
    }

    @Nonnull
    public AstExp visit(AstPut e) {
        AstExp Old_Expr1 = e.array;
        AstExp Old_Expr2 = e.index;
        AstExp Old_Expr3 = e.value;
        AstExp New_Expr1 = Old_Expr1.accept(this);
        AstExp New_Expr2 = Old_Expr2.accept(this);
        AstExp New_Expr3 = Old_Expr3.accept(this);
        if (New_Expr1 == e.array && New_Expr2 == e.index && New_Expr3 == e.value) return e;
        return new AstPut(New_Expr1, New_Expr2, New_Expr3);
    }

    //Arity = n. Comparing all new nodes in the list with the old list.
    
    @Nonnull
    public AstExp visit(AstApp e) {
        AstExp Old_Expr = e.e;
        AstExp New_Expr = Old_Expr.accept(this);
        List<AstExp> new_args = new ArrayList<>(e.es.size());
        for (AstExp old_arg : e.es) {
            new_args.add(old_arg.accept(this));
        }
        if (New_Expr.equals(e.e) && new_args.equals(e.es)) return e;
        return new AstApp(New_Expr, new_args);
    }

    @Nonnull
    public AstExp visit(AstTuple e) {
        List<AstExp> new_items = new ArrayList<>(e.es.size());
        for (AstExp old_item : e.es) {
            new_items.add(old_item.accept(this));
        }
        if (new_items.equals(e.es)) return e;
        return new AstTuple(new_items);
    }

    @Nonnull
    public AstExp visit(AstFunDef e) {
        AstExp Old_Expr = e.body;
        AstExp New_Expr = Old_Expr.accept(this);
        if (New_Expr == Old_Expr) return e;
        return new AstFunDef(e.decl, e.args, New_Expr);
    }

    @Nonnull
    public AstExp visit(AstLetRec e) {
        AstExp Old_Expr = e.ret;
        AstExp New_Expr = Old_Expr.accept(this);
        AstFunDef old_fd = e.fd;
        AstFunDef new_fd = (AstFunDef) old_fd.accept(this);
        if (new_fd == e.fd && New_Expr == e.ret) return e;
        return new AstLetRec(new_fd, New_Expr);
    }

}
