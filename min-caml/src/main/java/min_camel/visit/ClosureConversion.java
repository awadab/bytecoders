package min_camel.visit;

import min_camel.knorm.*;
import min_camel.type.Type;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.*;

@ParametersAreNonnullByDefault
@SuppressWarnings("unused")
public final class ClosureConversion extends KnormTransformHelper {

    Map<String, KnormFuncDefinition> topLvl = new LinkedHashMap<>();
    Set<String> known = new HashSet<>();
    Set<String> Environment = new HashSet<>();

    private ClosureConversion() {
    }

    public static Program execute(KnormNode root, Set<String> globals) {
        ClosureConversion cc = new ClosureConversion();
        KnormNode result;

        cc.known.addAll(globals);
        result = root.accept(cc);

        return new Program(cc.topLvl, result);
    }

    private static Program execute(KnormNode root) {
        return execute(root, Collections.<String>emptySet());
    }

    @Nonnull
    public KnormNode visit(KnormLetRec e) {
        KnormNode e1_New, e2_New;
        String id = e.FuncDef.FuncName.id;
        List<SymRef> e1_free = new LinkedList<>(e.FuncDef.FreeVars);
        Iterator<SymRef> iterator;

        iterator = e1_free.iterator();
        while (iterator.hasNext()) {
            SymRef ref = iterator.next();
            if (known.contains(ref.id)) {
                iterator.remove();
            }
        }

        if (e1_free.isEmpty()) {
            known.add(id);
        }

        KnormFuncDefinition fd = new KnormFuncDefinition(
                e.FuncDef.FuncName, e.FuncDef.FuncArgs,
                e.FuncDef.FuncBody.accept(this)
        );
        topLvl.put(id, fd);

        e2_New = e.ret.accept(this);

        List<SymRef> e2_free = KnormFreeVars.compute(e2_New);
        iterator = e2_free.iterator();
        while (iterator.hasNext()) {
            SymRef ref = iterator.next();
            if (!ref.id.equals(id)) {
                continue;
            }
            iterator.remove();
            e2_New = new KnormClosureMake(
                    new SymDef(id, Type.generate()), // fixme: type
                    fd.FuncName.makeRef(),
                    e1_free,
                    e2_New
            );
            break;
        }

        return e2_New;

    }

    @Nonnull
    public KnormNode visit(KnormApplication e) {
        SymRef f = e.symbRef;
        if (known.contains(f.id)) {
            return new KnormDirect(f, e.args);
        }
        return new KnormCallFunc(f, e.args);
    }

}
