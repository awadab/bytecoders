package min_camel.visit;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import min_camel.ast.AstExp;
import min_camel.ast.AstFunDef;
import min_camel.ast.AstLetTuple;
import min_camel.helpers.SymDef;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.*;

@ParametersAreNonnullByDefault
public final class DuplicateDeclaration extends DummyVisitor {

    private List<Collection<SymDef>> findings = new LinkedList<>();

    private Multimap<String, SymDef> MultMap = HashMultimap.create();

    private DuplicateDeclaration() {
    }

    public static List<Collection<SymDef>> Execute(AstExp root) {
        DuplicateDeclaration dd = new DuplicateDeclaration();
        root.accept(dd);
        return dd.findings;
    }

    private void CheckDefinitions(List<SymDef> defs) {
        for (SymDef def : defs) {
            MultMap.put(def.id, def);
        }
        for (String entry : MultMap.keySet()) {
            Collection<SymDef> occurrences = MultMap.get(entry);
            if (occurrences.size() > 1) {
                findings.add(new ArrayList<>(occurrences));
            }
        }
        MultMap.clear();
    }

    public void visit(AstLetTuple e) {
        CheckDefinitions(e.ids);
    }

    public void visit(AstFunDef e) {
        CheckDefinitions(e.args);
    }

}
