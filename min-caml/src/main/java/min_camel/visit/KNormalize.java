package min_camel.visit;

import min_camel.ast.*;
import min_camel.knorm.*;
import min_camel.type.TupleType;
import min_camel.type.Type;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;
import min_camel.helpers.SymbolTable;
import min_camel.helpers.Visitor1;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

/**
 * Execute K-Normalization that transform an AST to a simplified AST on which
 * operations are executed on variables and not sub-expressions. Complex
 * expressions
 * are divided into multiple nested let expressions.
 */
@ParametersAreNonnullByDefault
@SuppressWarnings("unused")
public final class KNormalize extends KNormalizeHelper
        implements Visitor1<KnormNode> {

    private SymbolTable<Type> SymTable = new SymbolTable<>();

    private KNormalize() {
    }

    public static KnormNode Execute(AstExp root, Map<String, Type> globals) {
        KNormalize kn = new KNormalize();
        kn.SymTable.push();
        kn.SymTable.top().putAll(globals);
        return root.accept(kn);
    }

    public static KnormNode Execute(AstExp parseTree) {
        return Execute(parseTree, Collections.<String, Type>emptyMap());
    }

    public KnormNode visit(AstUnit e) {
        return KnormUnit.INSTANCE;
    }

    public KnormNode visit(AstBool e) {
        return e.b ? KnormInt.CONST_1 : KnormInt.CONST_0;
    }

    public KnormNode visit(AstInt e) {
        return new KnormInt(e.i);
    }

    public KnormNode visit(AstFloat e) {
        return new KnormFloat(e.f);
    }

    public KnormNode visit(AstNot e) {
        AstIf astIf = new AstIf(e.e, new AstBool(false), new AstBool(true));
        return astIf.accept(this);
    }

    public KnormNode visit(AstNeg e) {
        return insert_let1(
                e.e.accept(this),
                HANDLE_NEG_I);
    }

    public KnormNode visit(AstAdd e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_ADD_I);
    }

    public KnormNode visit(AstSub e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_SUB_I);
    }

    public KnormNode visit(AstFNeg e) {
        return insert_let1(
                e.e.accept(this),
                HANDLE_NEG_F);
    }

    public KnormNode visit(AstFAdd e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_ADD_F);
    }

    public KnormNode visit(AstFSub e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_SUB_F);
    }

    public KnormNode visit(AstFMul e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_MUL_F);
    }

    public KnormNode visit(AstFDiv e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_DIV_F);
    }

    public KnormNode visit(AstEq e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_EQ);
    }

    public KnormNode visit(AstLE e) {
        return insert_let2(
                e.e1.accept(this),
                e.e2.accept(this),
                HANDLE_LE);
    }

    public KnormNode visit(final AstIf e) {
        final AstExp cond = e.eCond;

        if (cond instanceof AstBool) {
            return (((AstBool) cond).b ? e.eThen : e.eElse).accept(this);
        }

        if (cond instanceof AstNeg) {
            AstExp astIf = new AstIf(((AstNeg) cond).e, e.eElse, e.eThen);
            return astIf.accept(this);
        }

        final KnormNode kThen = e.eThen.accept(this);
        final KnormNode kElse = e.eElse.accept(this);

        AstExp op1 = null, op2 = null;
        boolean isEq, isLe = false;

        if (isEq = (cond instanceof AstEq)) {
            AstEq eq = ((AstEq) cond);
            op1 = eq.e1;
            op2 = eq.e2;
        } else if (isLe = (cond instanceof AstLE)) {
            AstLE le = ((AstLE) cond);
            op1 = le.e1;
            op2 = le.e2;
        }

        if (isEq || isLe) {
            Handler2 handler = new Handler2() {
                public KnormNode apply(SymRef var1, SymRef var2) {
                    return ((cond instanceof AstEq)
                            ? new KnormIfEqual(var1, var2, kThen, kElse)
                            : new KnormIfLess(var1, var2, kThen, kElse));
                }
            };
            return insert_let2(
                    op1.accept(this),
                    op2.accept(this),
                    handler);
        }

        return insert_let2(
                e.eCond.accept(this),
                KnormInt.CONST_0,
                new Handler2() {
                    public KnormNode apply(SymRef var, SymRef const0) {
                        return new KnormIfEqual(var, const0, kElse, kThen);
                    }
                });
    }

    public KnormNode visit(AstLet e) {
        KnormNode initializer;
        KnormNode ret;
        SymTable.push();
        {
            initializer = e.initializer.accept(this);
            SymTable.put(e.decl.id, e.decl.type);
            ret = e.ret.accept(this);
        }
        SymTable.pop();
        return new KnormLet(e.decl, initializer, ret);
    }

    public KnormNode visit(SymRef e) {
        Type t = SymTable.get(e.id);
        if (t == null) {
            // This never happens after checking for free variables.
            // But we still create a new type variable.
            t = Type.generate();
        }
        return new KnormVar(e, t);
    }

    public KnormNode visit(AstLetRec e) {
        KnormNode ret;
        KnormNode fdBody;
        SymTable.push();
        {
            SymTable.put(e.fd.decl.id, e.fd.decl.type);
            SymTable.push();
            {
                for (SymDef arg : e.fd.args) {
                    SymTable.put(arg.id, arg.type);
                }
                fdBody = e.fd.body.accept(this);
            }
            SymTable.pop();
            ret = e.ret.accept(this);
        }
        SymTable.pop();

        return new KnormLetRec(new KnormFuncDefinition(e.fd.decl, e.fd.args, fdBody), ret);
    }

    public KnormNode visit(AstApp e) {
        final int n = e.es.size();
        final KnormNode[] arguments = new KnormNode[n];

        final KnormNode callee = e.e.accept(this);

        for (int i = 0; i < n; ++i) {
            arguments[i] = e.es.get(i).accept(this);
        }

        return insert_let1(callee, new Handler1() {
            public KnormNode apply(final SymRef functionVar) {
                return insert_letN(arguments, new HandlerN() {
                    public KnormNode apply(SymRef[] vars) {
                        return new KnormApplication(
                                functionVar,
                                Arrays.asList(vars),
                                callee.getDataType());
                    }
                });
            }
        });

    }

    public KnormNode visit(AstTuple e) {
        int n = e.es.size();
        KnormNode[] items = new KnormNode[n];
        Type[] types = new Type[n];

        for (int i = 0; i < n; ++i) {
            items[i] = e.es.get(i).accept(this);
            types[i] = items[i].getDataType();
        }

        final TupleType type = new TupleType(Arrays.asList(types));

        return insert_letN(items, new HandlerN() {
            public KnormNode apply(SymRef[] vars) {
                return new KnormTuple(Arrays.asList(vars), type);
            }
        });
    }

    public KnormNode visit(final AstLetTuple e) {
        KnormNode kInit = e.initializer.accept(this);
        Handler1 handler = new Handler1() {
            public KnormNode apply(SymRef varInitializer) {
                KnormNode ret;
                SymTable.push();
                {
                    for (SymDef id : e.ids) {
                        SymTable.put(id.id, id.type);
                    }
                    ret = e.ret.accept(KNormalize.this);
                }
                SymTable.pop();

                return new KnormLetTuple(e.ids, varInitializer, ret);
            }
        };
        return insert_let1(kInit, handler);
    }

    public KnormNode visit(AstArray e) {
        final KnormNode size = e.size.accept(this);
        final KnormNode init = e.initializer.accept(this);
        final Handler2 handler = new Handler2() {
            public KnormNode apply(SymRef var1, SymRef var2) {
                return new KnormArr(var1, var2, init.getDataType());
            }
        };
        return insert_let2(size, init, handler);
    }

    public KnormNode visit(AstGet e) {
        final KnormNode kArray = e.array.accept(this);
        final KnormNode kIndex = e.index.accept(this);
        final Handler2 handler = new Handler2() {
            public KnormNode apply(SymRef var1, SymRef var2) {
                return new KnormGet(var1, var2, kArray.getDataType());
            }
        };
        return insert_let2(kArray, kIndex, handler);
    }

    public KnormNode visit(AstPut e) {
        return insert_let3(
                e.array.accept(this),
                e.index.accept(this),
                e.value.accept(this),
                HANDLE_PUT);
    }

    public KnormNode visit(AstFunDef e) {
        throw new RuntimeException("" +
                "Can't apply K-normalization directly on " +
                "an `AstFunDef` object. Apply the transform on " +
                "the containing `AstLetRec` instead.");
    }

    public KnormNode visit(AstErr e) {
        throw new RuntimeException("" +
                "Can't apply K-normalization on an " +
                "`AstErr` object. This operation should not be done " +
                "if there were parse errors.");
    }

}
