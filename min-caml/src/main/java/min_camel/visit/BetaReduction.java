package min_camel.visit;

import min_camel.ast.AstExp;
import min_camel.ast.AstLet;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;
import min_camel.helpers.SymbolTable;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public final class BetaReduction extends TransformHelper {

    private SymbolTable<SymRef> reMap = new SymbolTable<>();

    private BetaReduction() {
    }

    @Nonnull
    public static AstExp compute(AstExp astNode) {
        return astNode.accept(new BetaReduction());
    }


    @Nonnull
    public AstExp visit(SymRef e) {
        AstExp new_e = reMap.get(e.id);
        return (new_e == null) ? e : new_e;
    }


    @Nonnull
    public AstExp visit(AstLet e) {
        SymDef Old_Id = e.decl;
        AstExp New_Expr1 = e.initializer.accept(this);
        AstExp New_Expr2;

        if (New_Expr1 instanceof SymRef) {
            reMap.push();
            {
                reMap.put(Old_Id.id, (SymRef) New_Expr1);
                New_Expr2 = e.ret.accept(this);
            }
            reMap.pop();
        } else {
            New_Expr2 = e.ret.accept(this);
        }

        return new AstLet(Old_Id, New_Expr1, New_Expr2);
    }


}
