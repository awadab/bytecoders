package min_camel.visit;

import min_camel.knorm.*;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
abstract class KNormalizeHelper {

    private int x = 0;

    protected SymDef newSymDef(KnormNode normalized) {
        return new SymDef(
                "tmp" + (++x),
                normalized.getDataType()
        );
    }

    protected static interface Handler1 {
        KnormNode apply(SymRef var);
    }

    protected static interface Handler2 {
        KnormNode apply(SymRef var1, SymRef var2);
    }

    protected static interface Handler3 {
        KnormNode apply(SymRef var1, SymRef var2, SymRef var3);
    }

    protected static interface HandlerN {
        KnormNode apply(SymRef[] vars);
    }

    protected final static Handler1 HANDLE_NEG_I = new Handler1() {
        public KnormNode apply(SymRef var) {
            return new KnormNeg(var);
        }
    };

    protected final static Handler2 HANDLE_ADD_I = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormAdd(var1, var2);
        }
    };

    protected final static Handler2 HANDLE_SUB_I = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormSub(var1, var2);
        }
    };

    protected final static Handler1 HANDLE_NEG_F = new Handler1() {
        public KnormNode apply(SymRef var) {
            return new KnormFloatNeg(var);
        }
    };

    protected final static Handler2 HANDLE_ADD_F = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormFloatAdd(var1, var2);
        }
    };

    protected final static Handler2 HANDLE_SUB_F = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormFloatSub(var1, var2);
        }
    };

    protected final static Handler2 HANDLE_MUL_F = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormFloatMul(var1, var2);
        }
    };

    protected final static Handler2 HANDLE_DIV_F = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormFloatDiv(var1, var2);
        }
    };

    protected final static Handler2 HANDLE_EQ = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormIfEqual(var1, var2, KnormInt.CONST_1, KnormInt.CONST_0);
        }
    };

    protected final static Handler2 HANDLE_LE = new Handler2() {
        public KnormNode apply(SymRef var1, SymRef var2) {
            return new KnormIfLess(var1, var2, KnormInt.CONST_1, KnormInt.CONST_0);
        }
    };

    protected final static Handler3 HANDLE_PUT = new Handler3() {
        @Override
        public KnormNode apply(SymRef var1, SymRef var2, SymRef var3) {
            return new KnormPut(var1, var2, var3);
        }
    };

    @Nonnull
    protected KnormNode insert_let1(
            KnormNode normalized,
            Handler1 handler
    ) {
        if (normalized instanceof KnormVar) {
            return handler.apply(((KnormVar) normalized).Variable);
        }

        SymDef destVar = newSymDef(normalized);

        return new KnormLet(
                destVar,
                normalized,
                handler.apply(new SymRef(destVar.id))
        );
    }

    @Nonnull
    protected KnormNode insert_let2(
            KnormNode normalized1,
            KnormNode normalized2,
            Handler2 handler
    ) {
        SymDef letSymbol1;
        SymDef letSymbol2;
        SymRef result1;
        SymRef result2;
        KnormNode handledResult;

        if ((normalized1 instanceof KnormVar)) {
            letSymbol1 = null;
            result1 = ((KnormVar) normalized1).Variable;
        } else {
            letSymbol1 = newSymDef(normalized1);
            result1 = letSymbol1.makeRef();
        }

        if ((normalized2 instanceof KnormVar)) {
            letSymbol2 = null;
            result2 = ((KnormVar) normalized2).Variable;
        } else {
            letSymbol2 = newSymDef(normalized2);
            result2 = letSymbol2.makeRef();
        }

        handledResult = handler.apply(result1, result2);

        if (letSymbol1 != null) {
            handledResult = new KnormLet(
                    letSymbol1,
                    normalized1,
                    handledResult
            );
        }

        if (letSymbol2 != null) {
            handledResult = new KnormLet(
                    letSymbol2,
                    normalized2,
                    handledResult
            );
        }

        return handledResult;
    }

    @Nonnull
    protected KnormNode insert_let3(
            KnormNode normalized1,
            KnormNode normalized2,
            KnormNode normalized3,
            Handler3 handler
    ) {
        SymDef letSymbol1;
        SymDef letSymbol2;
        SymDef letSymbol3;
        SymRef result1;
        SymRef result2;
        SymRef result3;
        KnormNode handledResult;

        if ((normalized1 instanceof KnormVar)) {
            letSymbol1 = null;
            result1 = ((KnormVar) normalized1).Variable;
        } else {
            letSymbol1 = newSymDef(normalized1);
            result1 = letSymbol1.makeRef();
        }

        if ((normalized2 instanceof KnormVar)) {
            letSymbol2 = null;
            result2 = ((KnormVar) normalized2).Variable;
        } else {
            letSymbol2 = newSymDef(normalized2);
            result2 = letSymbol2.makeRef();
        }

        if ((normalized3 instanceof KnormVar)) {
            letSymbol3 = null;
            result3 = ((KnormVar) normalized3).Variable;
        } else {
            letSymbol3 = newSymDef(normalized3);
            result3 = letSymbol3.makeRef();
        }

        handledResult = handler.apply(result1, result2, result3);

        if (letSymbol1 != null) {
            handledResult = new KnormLet(
                    letSymbol1,
                    normalized1,
                    handledResult
            );
        }

        if (letSymbol2 != null) {
            handledResult = new KnormLet(
                    letSymbol2,
                    normalized2,
                    handledResult
            );
        }

        if (letSymbol3 != null) {
            handledResult = new KnormLet(
                    letSymbol3,
                    normalized3,
                    handledResult
            );
        }

        return handledResult;
    }

    @Nonnull
    protected KnormNode insert_letN(
            KnormNode[] normalized,
            HandlerN handler
    ) {
        int n = normalized.length;
        SymDef[] letSymbols = new SymDef[n];
        SymRef[] results = new SymRef[n];
        KnormNode handledResult;

        for (int i = 0; i < n; ++i) {
            KnormNode node = normalized[i];
            if (node instanceof KnormVar) {
                results[i] = ((KnormVar) node).Variable;
            } else {
                letSymbols[i] = newSymDef(node);
                results[i] = letSymbols[i].makeRef();
            }
        }

        handledResult = handler.apply(results);

        for (int i = 0; i < n; ++i) {
            if (letSymbols[i] == null) continue;
            handledResult = new KnormLet(
                    letSymbols[i],
                    normalized[i],
                    handledResult
            );
        }

        return handledResult;
    }
}
