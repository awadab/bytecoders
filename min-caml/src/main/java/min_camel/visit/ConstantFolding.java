package min_camel.visit;

import min_camel.ast.*;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;
import min_camel.helpers.SymbolTable;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public final class ConstantFolding extends TransformHelper {

    private SymbolTable<AstExp> reMap = new SymbolTable<>();

    private ConstantFolding() {
    }

    public static AstExp compute(AstExp astNode) {
        return astNode.accept(new ConstantFolding());
    }

    // return an AstInt of value the value of the variable in the symbol
    // table. Ex: return 8 if we have y = 8 in symTable. Otherwise skip
    @Nonnull
    public AstExp visit(SymRef e) {
        AstExp New_Exprxpr = reMap.get(e.id);
        return (New_Exprxpr == null) ? e : New_Exprxpr;
    }

    @Nonnull
    public AstExp visit(AstLet e) {
        SymDef Ols_Id = e.decl;
        AstExp New_Expr1 = e.initializer.accept(this);
        AstExp New_Expr2;

        reMap.push();
        {
            // Puts the mapping e.id -> e.e1 (its value) in the stack and transforms the
            // expression e.e2
            if (isSuitable(New_Expr1)) {
                reMap.put(Ols_Id.id, New_Expr1);
            }

            New_Expr2 = e.ret.accept(this);
        }
        reMap.pop();

        return new AstLet(Ols_Id, New_Expr1, New_Expr2);
    }

    private boolean isSuitable(AstExp e) {
        if (e instanceof AstUnit)
            return true;
        if (e instanceof AstInt)
            return true;
        if (e instanceof AstArray)
            return true;
        if (e instanceof AstBool)
            return true;
        if (e instanceof AstFloat)
            return true;
        if (e instanceof AstTuple)
            return true;
        if (e instanceof SymRef)
            return true;
        return false;
    }

    @Nonnull
    public AstExp visit(AstAdd e) {
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);

        if (New_Expr1 instanceof AstInt && New_Expr2 instanceof AstInt) {
            return new AstInt(
                    ((AstInt) New_Expr1).i + ((AstInt) New_Expr2).i);
        }

        return new AstAdd(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstSub e) {
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);

        if (New_Expr1 instanceof AstInt && New_Expr2 instanceof AstInt) {
            int val = ((AstInt) New_Expr1).i - ((AstInt) New_Expr2).i;
            return new AstInt(val);
        }

        return new AstSub(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstNeg e) {
        AstExp New_Expr = e.e.accept(this);

        if (New_Expr instanceof AstInt) {
            return new AstInt(-((AstInt) New_Expr).i);
        }

        return new AstNeg(New_Expr);
    }

    @Nonnull
    public AstExp visit(AstFNeg e) {
        AstExp New_Expr = e.e.accept(this);

        if (New_Expr instanceof AstFloat) {
            return new AstFloat(-((AstFloat) New_Expr).f);
        }

        return new AstNeg(New_Expr);
    }

    @Nonnull
    public AstExp visit(AstFAdd e) {
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);

        if (New_Expr1 instanceof AstFloat && New_Expr2 instanceof AstFloat) {
            return new AstFloat(
                    ((AstFloat) New_Expr1).f + ((AstFloat) New_Expr2).f);
        }

        return new AstFAdd(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstFSub e) {
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);

        if (New_Expr1 instanceof AstFloat && New_Expr2 instanceof AstFloat) {
            return new AstFloat(
                    ((AstFloat) New_Expr1).f - ((AstFloat) New_Expr2).f);
        }

        return new AstFSub(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstFMul e) {
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);

        if (New_Expr1 instanceof AstFloat && New_Expr2 instanceof AstFloat) {
            return new AstFloat(
                    ((AstFloat) New_Expr1).f * ((AstFloat) New_Expr2).f);
        }

        return new AstFMul(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstFDiv e) {
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);

        if (New_Expr1 instanceof AstFloat && New_Expr2 instanceof AstFloat) {
            return new AstFloat(
                    ((AstFloat) New_Expr1).f / ((AstFloat) New_Expr2).f);
        }

        return new AstFMul(New_Expr1, New_Expr2);
    }

    @Nonnull
    public AstExp visit(AstNot e) {
        AstExp New_Expr = e.e.accept(this);

        if (New_Expr instanceof AstBool) {
            return new AstBool(!((AstBool) New_Expr).b);
        }
        return new AstNot(New_Expr);
    }

    @Nonnull
    public AstExp visit(AstIf e) {
        AstExp New_Expr1 = e.eCond.accept(this);
        if (New_Expr1 instanceof AstBool) {
            if (((AstBool) New_Expr1).b) {
                return e.eThen.accept(this);
            } else {
                return e.eElse.accept(this);
            }
        }

        AstExp New_Expr2 = e.eThen.accept(this);
        AstExp New_Expr3 = e.eElse.accept(this);
        return new AstIf(New_Expr1, New_Expr2, New_Expr3);

    }

    @Nonnull
    public AstExp visit(AstEq e) {
        boolean b;
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);
        if (New_Expr1 instanceof AstInt && New_Expr2 instanceof AstInt) {
            b = ((AstInt) New_Expr1).i == ((AstInt) New_Expr2).i;
        } else if (New_Expr1 instanceof AstBool && New_Expr2 instanceof AstBool) {
            b = ((AstBool) New_Expr1).b == ((AstBool) New_Expr2).b;
        } else if (New_Expr1 instanceof AstFloat && New_Expr2 instanceof AstFloat) {
            b = ((AstFloat) New_Expr1).f == ((AstFloat) New_Expr2).f;
        } else {
            return new AstEq(New_Expr1, New_Expr2);
        }

        return AstBool.staticInstance(b);

    }

    @Nonnull
    public AstExp visit(AstLE e) {
        boolean bool;
        AstExp New_Expr1 = e.e1.accept(this);
        AstExp New_Expr2 = e.e2.accept(this);

        if (New_Expr1 instanceof AstInt && New_Expr2 instanceof AstInt) {
            bool = ((AstInt) New_Expr1).i <= ((AstInt) New_Expr2).i;
        } else if (New_Expr1 instanceof AstFloat && New_Expr2 instanceof AstFloat) {
            bool = ((AstFloat) New_Expr1).f <= ((AstFloat) New_Expr2).f;
        } else {
            return new AstLE(New_Expr1, New_Expr2);
        }

        return AstBool.staticInstance(bool);

    }

}
