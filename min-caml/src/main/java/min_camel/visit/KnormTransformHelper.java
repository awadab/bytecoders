package min_camel.visit;

import min_camel.knorm.*;
import min_camel.helpers.KnormVisitor1;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public abstract class KnormTransformHelper implements KnormVisitor1<KnormNode> {

    @Nonnull
    public KnormNode visit(KnormUnit k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormInt k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormFloat k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormVar k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormTuple k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormArr k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormNeg k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormAdd k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormSub k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormFloatNeg k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormFloatAdd k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormFloatDiv k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormFloatMul k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormFloatSub k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormGet k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormPut k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormLet k) {
        KnormNode new_init = k.init.accept(this);
        KnormNode new_ret = k.ret.accept(this);
        if (new_init == k.init && new_ret == k.ret) return k;
        return new KnormLet(k.Id, new_init, new_ret);
    }

    @Nonnull
    public KnormNode visit(KnormLetRec k) {
        KnormNode new_body = k.FuncDef.FuncBody.accept(this);
        KnormNode new_ret = k.ret.accept(this);
        KnormFuncDefinition new_fd = ((new_body == k.FuncDef.FuncBody)
                ? new KnormFuncDefinition(k.FuncDef.FuncName, k.FuncDef.FuncArgs, new_body)
                : k.FuncDef
        );
        if (new_fd == k.FuncDef && new_ret == k.ret) return k;
        return new KnormLetRec(new_fd, new_ret);
    }

    @Nonnull
    public KnormNode visit(KnormLetTuple k) {
        KnormNode new_ret = k.ret.accept(this);
        if (new_ret == k.ret) return k;
        return new KnormLetTuple(k.Ids, k.init, new_ret);
    }

    @Nonnull
    public KnormNode visit(KnormIfEqual k) {
        KnormNode new_then = k.kThen.accept(this);
        KnormNode new_else = k.kElse.accept(this);
        if (new_then == k.kThen && new_else == k.kElse) return k;
        return new KnormIfEqual(k.opr1, k.opr2, new_then, new_else);
    }

    @Nonnull
    public KnormNode visit(KnormIfLess k) {
        KnormNode new_then = k.kThen.accept(this);
        KnormNode new_else = k.kElse.accept(this);
        if (new_then == k.kThen && new_else == k.kElse) return k;
        return new KnormIfLess(k.opr1, k.opr2, new_then, new_else);
    }

    @Nonnull
    public KnormNode visit(KnormApplication k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormCallFunc k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormDirect k) {
        return k;
    }

    @Nonnull
    public KnormNode visit(KnormClosureMake k) {
        return k;
    }
}
