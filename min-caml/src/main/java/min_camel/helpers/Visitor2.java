package min_camel.helpers;

import min_camel.ast.*;

import javax.annotation.Nonnull;

/**
 * This is the same as the visitor interface but in this interface we are
 * using generics for the return type and the argument type.
 */
public interface Visitor2<ReturnType, ArgumentType>{
    ReturnType visit(ArgumentType a, @Nonnull AstUnit e);
    ReturnType visit(ArgumentType a, @Nonnull AstBool e);
    ReturnType visit(ArgumentType a, @Nonnull AstInt e);
    ReturnType visit(ArgumentType a, @Nonnull AstFloat e);
    ReturnType visit(ArgumentType a, @Nonnull AstNot e);
    ReturnType visit(ArgumentType a, @Nonnull AstNeg e);
    ReturnType visit(ArgumentType a, @Nonnull AstAdd e);
    ReturnType visit(ArgumentType a, @Nonnull AstFAdd e);
    ReturnType visit(ArgumentType a, @Nonnull AstSub e);
    ReturnType visit(ArgumentType a, @Nonnull AstFSub e);
    ReturnType visit(ArgumentType a, @Nonnull AstFNeg e); 
    ReturnType visit(ArgumentType a, @Nonnull AstFMul e);
    ReturnType visit(ArgumentType a, @Nonnull AstFDiv e);
    ReturnType visit(ArgumentType a, @Nonnull AstArray e);
    ReturnType visit(ArgumentType a, @Nonnull AstGet e);
    ReturnType visit(ArgumentType a, @Nonnull AstPut e);
    ReturnType visit(ArgumentType a, @Nonnull AstEq e);
    ReturnType visit(ArgumentType a, @Nonnull AstLE e);
    ReturnType visit(ArgumentType a, @Nonnull AstIf e);
    ReturnType visit(ArgumentType a, @Nonnull AstLet e);
    ReturnType visit(ArgumentType a, @Nonnull SymRef e);
    ReturnType visit(ArgumentType a, @Nonnull AstLetRec e);
    ReturnType visit(ArgumentType a, @Nonnull AstApp e);
    ReturnType visit(ArgumentType a, @Nonnull AstTuple e);
    ReturnType visit(ArgumentType a, @Nonnull AstLetTuple e);
    ReturnType visit(ArgumentType a, @Nonnull AstFunDef e);
    ReturnType visit(ArgumentType a, @Nonnull AstErr e);
}


