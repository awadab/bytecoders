package min_camel.helpers;

import min_camel.knorm.*;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public interface KnormVisitor {
    // Values
    void visit(KnormUnit k);
    void visit(KnormInt k);
    void visit(KnormFloat k);
    void visit(KnormVar k);
    void visit(KnormTuple k);
    void visit(KnormArr k);

    // Integer operations
    void visit(KnormNeg k);
    void visit(KnormAdd k);
    void visit(KnormSub k);

    // Floating point operations
    void visit(KnormFloatNeg k);
    void visit(KnormFloatAdd k);
    void visit(KnormFloatSub k);
    void visit(KnormFloatDiv k);
    void visit(KnormFloatMul k);


    // Array operations
    void visit(KnormGet k);
    void visit(KnormPut k);

    // Let
    void visit(KnormLet k);
    void visit(KnormLetRec k);
    void visit(KnormLetTuple k);

    // Branch
    void visit(KnormIfEqual k);
    void visit(KnormIfLess k);

    // Calling functions (only used in K-normalized code)
    void visit(KnormApplication k);

    // Calling functions (after closure-converting the K-normalized code)
    void visit(KnormCallFunc k);
    void visit(KnormDirect k);
    void visit(KnormClosureMake k);
}
