package min_camel.helpers;

import min_camel.knorm.*;

import javax.annotation.Nonnull;

public interface KnormVisitor2<T, U> {
    // Values
    T visit(U u, @Nonnull KnormUnit k);
    T visit(U u, @Nonnull KnormInt k);
    T visit(U u, @Nonnull KnormFloat k);
    T visit(U u, @Nonnull KnormVar k);
    T visit(U u, @Nonnull KnormTuple k);
    T visit(U u, @Nonnull KnormArr k);

    // Integer operations
    T visit(U u, @Nonnull KnormNeg k);
    T visit(U u, @Nonnull KnormAdd k);
    T visit(U u, @Nonnull KnormSub k);

    // Floating point operations
    T visit(U u, @Nonnull KnormFloatNeg k);
    T visit(U u, @Nonnull KnormFloatAdd k);
    T visit(U u, @Nonnull KnormFloatDiv k);
    T visit(U u, @Nonnull KnormFloatMul k);
    T visit(U u, @Nonnull KnormFloatSub k);

    // Array operations
    T visit(U u, @Nonnull KnormGet k);
    T visit(U u, @Nonnull KnormPut k);

    // Let
    T visit(U u, @Nonnull KnormLet k);
    T visit(U u, @Nonnull KnormLetRec k);
    T visit(U u, @Nonnull KnormLetTuple k);

    // Branch
    T visit(U u, @Nonnull KnormIfEqual k);
    T visit(U u, @Nonnull KnormIfLess k);

    // Calling functions (only used in K-normalized code)
    T visit(U u, @Nonnull KnormApplication k);

    // Calling functions (after closure-converting the K-normalized code)
    T visit(U u, @Nonnull KnormCallFunc k);
    T visit(U u, @Nonnull KnormDirect k);
    T visit(U u, @Nonnull KnormClosureMake k);
}
