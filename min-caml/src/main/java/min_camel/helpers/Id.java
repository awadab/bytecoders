package min_camel.helpers;

import min_camel.ast.AstNode;

import javax.annotation.Nonnull;

/**
 * These are the identifiers in the source code, they are used by the parser 
 * to get information about their location (from the Symbol object) in the source code 
 * along with their name.
 */
public final class Id extends AstNode {
    public final String id;

    public Id(String id) {
        this.id = id;
    }

    @Nonnull
    @Override
    public String toString() {
        return id;
    }

    static int x = -1;

    static public Id gen() {
        x++;
        return new Id("?v" + x);
    }
}
