package min_camel.helpers;

import java.util.Collection;

import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

public final class SymbolTable<T> {
    private Stack<Map<String, T>> stack;

    public SymbolTable() {
        this.stack = new Stack<>();
    }

    public void push() {
        stack.push(new TreeMap<String, T>());
    }

    public void pop() {
        stack.pop();
    }

    public void put(String name, T value) {
        Map<String, T> temp = stack.peek();
        temp.put(name, value);
    }

    public void putAll(Collection<? extends Map.Entry<String, T>> all) {
        Map<String, T> top = stack.peek();
        for (Map.Entry<String, T> e : all) {
            top.put(e.getKey(), e.getValue());
        }
    }

    public void putAll(Map<String, ? extends T> map) {
        stack.peek().putAll(map);
    }

    public Map<String, T> top() {
        return stack.peek();
    }

    private Map<String, T> getStackElem(int index) {

        if (index == 0)
            return stack.peek();


        Map<String, T> x = stack.pop();
        try {
            return getStackElem(index - 1);
        } finally {
            stack.push(x);
        }
    }

    public T get(String name) {
        Map<String, T> result;
        int i = 0;

        while (i < stack.size()) {
            result = getStackElem(i);
            for (String key : result.keySet()) {
                if (key.equals(name)) {
                    return result.get(key);
                }
            }
            i++;
        }
        return null;
    }

    public void put(Map.Entry<String, T> e) {
        stack.peek().put(e.getKey(), e.getValue());
    }
}
