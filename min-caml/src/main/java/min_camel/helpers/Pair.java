package min_camel.helpers;

import javax.annotation.concurrent.Immutable;

@Immutable
public class Pair<T, V> {
    public final T left;
    public final V right;

    public Pair(T l, V r) {
        this.left = l;
        this.right = r;
    }
}
