package min_camel.helpers;

import min_camel.ast.*;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * This is the same as the Visitor interface but int this interface we are 
 * using generics to specify the return type.
 */
@ParametersAreNonnullByDefault
public interface Visitor1<ReturnType>{
    ReturnType visit(AstUnit e);
    ReturnType visit(AstBool e);
    ReturnType visit(AstInt e);
    ReturnType visit(AstFloat e);
    ReturnType visit(AstNot e);
    ReturnType visit(AstNeg e);
    ReturnType visit(AstAdd e);
    ReturnType visit(AstSub e);
    ReturnType visit(AstFNeg e);
    ReturnType visit(AstFAdd e);
    ReturnType visit(AstFSub e);
    ReturnType visit(AstFMul e);
    ReturnType visit(AstFDiv e);
    ReturnType visit(AstEq e);
    ReturnType visit(AstLE e);
    ReturnType visit(AstIf e);
    ReturnType visit(AstLet e);
    ReturnType visit(SymRef e);
    ReturnType visit(AstLetRec e);
    ReturnType visit(AstApp e);
    ReturnType visit(AstTuple e);
    ReturnType visit(AstLetTuple e);
    ReturnType visit(AstArray e);
    ReturnType visit(AstGet e);
    ReturnType visit(AstPut e);
    ReturnType visit(AstFunDef e);
    ReturnType visit(AstErr e);
}


