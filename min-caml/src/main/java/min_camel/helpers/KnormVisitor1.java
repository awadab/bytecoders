package min_camel.helpers;

import min_camel.knorm.*;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public interface KnormVisitor1<T> {
    // Values
    T visit(KnormUnit k);
    T visit(KnormInt k);
    T visit(KnormFloat k);
    T visit(KnormVar k);
    T visit(KnormTuple k);
    T visit(KnormArr k);

    // Integer operations
    T visit(KnormNeg k);
    T visit(KnormAdd k);
    T visit(KnormSub k);

    // Array operations
    T visit(KnormGet k);
    T visit(KnormPut k);

    // Floating point operations
    T visit(KnormFloatNeg k);
    T visit(KnormFloatAdd k);
    T visit(KnormFloatDiv k);
    T visit(KnormFloatMul k);
    T visit(KnormFloatSub k);
    
    // Let
    T visit(KnormLet k);
    T visit(KnormLetRec k);
    T visit(KnormLetTuple k);

    // Branch
    T visit(KnormIfEqual k);
    T visit(KnormIfLess k);

    // Calling functions (only used in K-normalized code)
    T visit(KnormApplication k);

    // Calling functions (after closure-converting the K-normalized code)
    T visit(KnormCallFunc k);
    T visit(KnormDirect k);
    T visit(KnormClosureMake k);

}
