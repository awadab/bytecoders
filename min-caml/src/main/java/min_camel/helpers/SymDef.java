package min_camel.helpers;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import min_camel.ast.AstNode;
import min_camel.type.Type;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Map;

/*
 * This class is the representation of a Symbol (identifier), it represents its name
 * and type
 */
@Immutable
@ParametersAreNonnullByDefault
public final class SymDef extends AstNode implements Map.Entry<String, Type> {
    
    // Symbol name
    @Nonnull
    public final String id;

    // Symbol type intialized from the Tvar class
    @Nonnull
    public final Type type;

    public SymDef(String id, Type type) {
        this.id = id;
        this.type = type;
    }

    public SymDef(Id id, Type type) {
        this(id.id, type);
        setSymbol(id.getSymbol());
    }

    public SymDef(String id) {
        this(id, Type.generate());
    }

    // From ID 
    public SymDef(Id id) {
        this(id.id, Type.generate());
        setSymbol(id.getSymbol());
    }

    @Nonnull
    public SymRef makeRef() {
        return new SymRef(id);
    }

    @Nonnull
    public static List<String> Id_List(List<SymDef> list) {
        return Lists.transform(list, new Function<SymDef, String>() {
            @Override
            public String apply(@Nullable SymDef from) {
                return from == null ? null : from.id;
            }
        });
    }

    @Nonnull
    public SymDef RenameSymbol(String newName) {
        if (newName.equals(id)) return this;
        SymDef sym = new SymDef(newName, type);
        sym.setSymbol(getSymbol());
        return sym;
    }
    
    @Nonnull
    public static List<Type> Types_List(List<SymDef> list) {
        return Lists.transform(list, new Function<SymDef, Type>() {
            @Override
            public Type apply(@Nullable SymDef from) {
                return from == null ? null : from.type;
            }
        });
    }

    public Type setValue(Type value) {
        throw new UnsupportedOperationException();
    }
    @Nonnull
    public String toString() {
        return id;
    }

    @Nonnull
    public String getKey() {
        return id;
    }

    @Nonnull
    public Type getValue() {
        return type;
    }

}
