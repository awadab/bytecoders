package min_camel.comp;

import min_camel.ir.Function;
import min_camel.ir.Instructions.*;
import min_camel.ir.opr.*;

import javax.annotation.Nonnull;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AssemblyGenerator {
    StringBuilder data;
    StringBuilder text;


    private static final Map<String, String> IMPORTS = new LinkedHashMap<>();

    private Map<String, Integer> argsOffset;
    private String[] regs;
    private ArrayList<String> memory;
    private int cursor;

    private String RETURN_KEYWORD = "ret"; // return value in r11
    private int heap_size = 2048; // To have 2048 cells of 4 bytes

    static {
        IMPORTS.put("print_newline", "min_caml_print_newline");
        IMPORTS.put("print_int", "min_caml_print_int");
        IMPORTS.put("int_of_float", "_min_caml_int_of_float");


    }


    public AssemblyGenerator() {
        //headers for .data and .text sections
        data = new StringBuilder("\t.data\n\t.balign 4");
        data.append("\nlimit : .word 0");    // to store the limit of the heap (head + heap_size*4)
        data.append("\nhead : .word 0");     // to have the head/top of the heap
        data.append("\nheap : .skip ").append(heap_size * 4); // to have heap_size cells of 4 bytes
        data.append("\nerror_message : .asciz\t\"Not enough space in the heap\\n\"");

        text = new StringBuilder("\n\t.text");
        text.append("\n\t.global _start");
        text.append("\n_start:");
        text.append("\n\tBL _main");
        text.append("\n\tBL min_caml_print_newline"); // for more clarity we print a new line before terminating correctly
        text.append("\n\tBL min_caml_exit\n");



    }

    public void GenAssemblyCode(List<Function> funcs) {
        for (Function FuncDef : funcs) {
            GenAssemblyCode(FuncDef);
        }
    }

    private void GenAssemblyCode(Function FuncDef) {
        int i, n;
        List<Var> args = FuncDef.args;
        String Var_Name;
        int Var_Offset;

        GenLabel(FuncDef.name);

        argsOffset = new LinkedHashMap<>();
        // initialization for the register allocator
        regs = new String[11];
        memory = new ArrayList<>();
        cursor = 3;


        // initialization of the heap management
        if(FuncDef.name.name.equals("_main")) {
            //push lr in stack
            text.append("\n\t@ prologue");
            text.append("\n\tSUB sp, #4");
            text.append("\n\tSTR lr, [sp]");

            //initialize the head of the heap in memory
            text.append("\n\tLDR r0, =heap");
            text.append("\n\tLDR r1, =head");
            text.append("\n\tSTR r0, [r1]");

            //initialize the limit of the heap in memory
            text.append("\n\tLDR r2, =limit");
            text.append("\n\tLDR r3, =").append(heap_size * 4);
            text.append("\n\tADD r3, r0, r3");
            text.append("\n\tSTR r3, [r2]");
        }
        else {
            // Function arguments
            n = args.size();
            if (n > 0) {
                text.append("\n\t@ arguments");
                for (i = 0; i < n; i++) {
                    Var_Name = args.get(i).name;
                    Var_Offset = 4 * i + 40;
                    text.append("\n\t@   ").append(Var_Name);
                    text.append(" -> r12");
                    text.append("+").append(Var_Offset);

                    argsOffset.put(Var_Name, Var_Offset);
                }
                text.append('\n');
            }

            // Prologue
            text.append("\n\t@ prologue");
            text.append("\n\tSUB sp, #4");
            text.append("\n\tSTR lr, [sp]");
            text.append("\n\tstmfd	sp!, {r3 - r10}");


            // Push frame pointer
            text.append("\n\tSUB sp, #4");
            text.append("\n\tSTR r12, [sp]");
            text.append("\n\tMOV r12, sp");
        }



        // Body of the function
        for (Instr instr : FuncDef.body) {
            Instr.Type t = instr.getInstrType();
            if (t != Instr.Type.LABEL) {
                text.append("\n\n\t@ ").append(instr.toString());
            }


            switch (t) {
                case CALL:
                    GenCallFunc((CallFunc) instr);
                    break;
                case CLOSURE_MAKE:
                    GenClosureMake((ClosureMake)instr);
                    break;
                case CLOSURE_APPLY:
                    GenClosureApply((ClosureApply)instr);
                    break;
                case ADD_I:
                    GenAddInt((AddI) instr);
                    break;
                case SUB_I:
                    GenSubInt((SubI) instr);
                    break;
                case ARRAY_NEW:
                    GenArrayCreate((ArrNew) instr);
                    break;
                case ARRAY_GET:
                    genGetArray((ArrGet) instr);
                    break;
                case ARRAY_PUT:
                    genPutArray((ArrPut) instr);
                    break;
                case ASSIGN:
                    GenAssign((Assign) instr);
                    break;
                case LABEL:
                    GenLabel((Label) instr);
                    break;
                case JUMP:
                    GenJump((Jump) instr);
                    break;
                case BRANCH:
                    GenBranch((Branch) instr);
                    break;
                case RETURN:
                    GenRet((Ret) instr);
                    break;

                default:
                    throw new RuntimeException(
                            "Generating assembly for " +
                                    instr.getInstrType() +
                                    " instructions not supported yet (" +
                                    instr.toString() + ")."
                    );
            }

        }

        // Epilogue
        text.append("\n\t@epilogue");
        if(!FuncDef.name.name.equals("_main")){
            text.append("\n\tMOV sp, r12");
            text.append("\n\tLDR r12, [sp]");
            text.append("\n\tADD sp, #4");
            text.append("\n\tldmfd	sp!, {r3 - r10}");
        }
        text.append("\n\tLDR lr, [sp]");
        text.append("\n\tADD sp, #4");
        text.append("\n\tMOV pc, lr\n\n");
    }

    private void GenArrayCreate(ArrNew instr) {
        text.append("\n@start make array");

        int r = GetNewRegister();
        regs[r] = instr.variable.name;
        String rd = "r" + r;

        ExecAssignment("r0", instr.size);
        text.append("\n\tBL malloc");
        text.append("\n\tMOV ").append(rd).append(", r0");

        if(instr.initialize != null) {
            //need to put the value init in every cell of the array in the heap
            ExecAssignment("r1", instr.initialize);
            ExecAssignment("r2", instr.size);
            text.append("\n\tMOV r0, ").append(rd);

            text.append("\nfor_make_array : ");
            text.append("\n\tCMP r2, #0");
            text.append("\n\tBLE end_for_make_array");
            text.append("\n\tSTR r1, [r0]");
            text.append("\n\tADD r0, r0, #4");
            text.append("\n\tSUB r2, r2, #1");
            text.append("\n\tBAL for_make_array");

            text.append("\nend_for_make_array : \n");
        }

        text.append("\n@end make array");
    }

    private void genPutArray(ArrPut instr) {
        text.append("\n@start put array");

        int reg = GetRegister(instr.arr.name);
        ExecAssignment("r0", instr.index);
        ExecAssignment("r1", instr.value);
        text.append("\n\tADD r0, r").append(reg).append(", r0, LSL #2");
        text.append("\n\tSTR r1, [r0]");

        text.append("\n@end put array");
    }

    private void genGetArray(ArrGet instr) {
        text.append("\n@start get array");
        String out = instr.out.name;
        int rd;
        int arr = GetRegister(instr.arr.name);
        if((rd = GetRegister(out))==-1){
            rd= GetNewRegister();
            regs[rd] = out;
        }
        ExecAssignment("r0", instr.index);

        text.append("\n\tADD r0, r").append(arr).append(", r0, LSL #2");
        text.append("\n\tSTR r").append(rd).append(", [r0]");

        text.append("\n@end get array");
    }

    private Integer locateVar(
            @Nonnull String name
    ) {
        Integer offset = argsOffset.get(name);
        return offset;
    }


    private int GetNewRegister(){

        for(int i = 3; i<=10; i++){
            if(regs[i] == null){
                return i;
            }
        }

        String tmp = regs[cursor];
        data.append("\n").append(tmp).append(" : .word ");
        text.append("\n\tLDR r0, =").append(tmp);
        text.append("\n\tSTR r").append(cursor).append(", [r0]");
        memory.add(tmp);

        int ret = cursor;
        cursor = cursor > 9 ? 3 : ++cursor;

        return ret;
    }


    private int GetRegister(String var){
        int i;

        if(var.equals(RETURN_KEYWORD)){
            return 11;
        }

        for (i = 3; i<11; i++){
            if(regs[i] != null && regs[i].equals(var)){
                return i;
            }
        }

        for(i = 0; i<memory.size(); i++){
            if(memory.get(i).equals(var)){
                memory.remove(i);
                int r = GetNewRegister();
                text.append("\n\tLDR r0, =").append(var);
                text.append("\n\tLDR r").append(r).append(", [r0]");
                regs[r] = var;

                return r;
            }
        }

        return -1;
    }



    private void ExecAssignment(@Nonnull Var v, @Nonnull Operand op) {
        String var = v.name;
        int rd;
        switch (op.getOperandType()) {
            case CONST_INT:
                rd = GetNewRegister();
                int val = ((ConstInt)op).value;
                regs[rd] = var;
                text.append("\n\tLDR r").append(rd).append(", =").append(val);
                return;


            case VAR :
                if((rd = GetRegister(var)) != -1){
                    ExecAssignment("r"+rd, op);
                }

                Integer offset = locateVar(var);
                if(offset != null) {
                    ExecAssignment("r0", op);
                    text.append("\n\tSTR r0, [r12, #").append(offset).append("]");
                    return;
                }

                // here the var is not encountered yet
                rd = GetNewRegister();
                regs[rd] = var;
                ExecAssignment("r"+rd,op);
                return;


        }

    }

    private void ExecAssignment(@Nonnull Var v, @Nonnull String register) {
        int rd;
        String var = v.name;

        if((rd = GetRegister(var)) != -1){
            text.append("\n\tMOV r").append(rd).append(", ").append(register);
            return;
        }

        Integer offset = locateVar(var);
        if(offset != null) {
            text.append("\n\tSTR ").append(register).append(", [r12, #").append(offset).append("]");
            return;
        }

        // here the var is not encountered yet
        rd = GetNewRegister();
        regs[rd] = var;
        text.append("\n\tMOV r").append(rd).append(", ").append(register);
        return;

    }

    private void ExecAssignment(@Nonnull String register, @Nonnull Operand op) {
        switch (op.getOperandType()) {
            case CONST_INT:
                text.append("\n\tLDR ").append(register);
                text.append(", =").append(((ConstInt) op).value);
                return;

            case CONST_FLOAT:
                // TODO: floats
                throw new RuntimeException(
                        "Support for floating point constants not implemented yet."
                );

            case LABEL:
                text.append("\n\tLDR ").append(register);
                text.append(", =").append(((Label)op).name);
                return;

            case VAR:
                int rd;
                String var = ((Var)op).name;

                if((rd = GetRegister(var)) != -1){
                    text.append("\n\tMOV ").append(register).append(", r").append(rd);
                    return;
                }

                Integer offset = locateVar(var);
                if(offset == null){
                    System.out.println(var);
                }
                text.append("\n\tLDR ").append(register).append(", [r12, #").append(offset).append("]");

        }
    }

    private void ExecAssignment(String destRegister, String srcRegister) {
        text.append("\n\tMOV ").append(destRegister).append(", ").append(srcRegister);
    }

    private void GenBinaryOperation(
            @Nonnull String memo,
            @Nonnull Var var,
            @Nonnull Operand opr1,
            @Nonnull Operand opr2
    ) {
        ExecAssignment("r1", opr1); // r1 <- opr1
        ExecAssignment("r2", opr2); // r2 <- opr2

        // r3 <- opr1 (?) opr2
        text.append("\n\t").append(memo).append(" r0, r1, r2");

        ExecAssignment(var, "r0"); // var <- r0
    }

    private void GenLabel(@Nonnull Label i) {
        text.append("\n").append(i.name).append(":");
    }

    private void GenCallFunc(@Nonnull CallFunc instr) {
        String name = instr.name;
        String New_name = IMPORTS.get(name);
        if (New_name != null) name = New_name;


        // EXCEPTION: print_int
        if (name.equals("min_caml_print_int")) {
            ExecAssignment("r0", instr.args.get(0)); // r0 <- only argument
            text.append("\n\tBL ").append(name); // just call

            if(instr.ret != null){
                ExecAssignment(instr.ret, "r11");
            }

            return;
        }

        if (name.equals("min_caml_print_newline")) {
            text.append("\n\tBL min_caml_print_newline");

            if(instr.ret != null){
                ExecAssignment(instr.ret, "r11");
            }
            return;
        }


        List<Operand> args = instr.args;
        for (int i = args.size() - 1; i >= 0; i--) {
            text.append("\n\t@ push argument ").append(i);
            text.append("\n\tSUB sp, #4");
            ExecAssignment("r0", args.get(i));
            text.append("\n\tSTR r0, [sp]");
        }

        text.append("\n\t@ call, free stack and set the return value");
        text.append("\n\tBL ").append(name);

        int popSize = 4 * instr.args.size();
        text.append("\n\tADD sp, #").append(Integer.toString(popSize));

        if(instr.ret != null){
            ExecAssignment(instr.ret, "r11");
        }

        text.append("\n\t@ end call");
    }

    private void GenClosureMake(@Nonnull ClosureMake instr) {
        List<Operand> args = instr.free_args;
        int var = GetNewRegister();
        String rd = "r" + var;
        regs[var] = instr.variable.name;

        text.append("\n@starting make cls");
        text.append("\n\tLDR r0, =").append(Integer.toString(args.size() + 2));
        text.append("\n\tBL malloc");
        text.append("\n\tMOV ").append(rd).append(", r0");

        text.append("\n\tLDR r2, =").append(Integer.toString(args.size()));
        text.append("\n\tSTR r2, [").append(rd).append("]");

        text.append("\n\tLDR r2, =").append(instr.fun.name);
        text.append("\n\tSTR r2, [").append(rd).append(", #4]");


        for(int i = 0; i<args.size(); i++){
            ExecAssignment("r2", args.get(i));
            text.append("\n\tSTR r2, [").append(rd).append(", #").append(Integer.toString(i*4 + 8)).append("]");
        }

        text.append("\n@ending make cls");
    }

    private void GenClosureApply(@Nonnull ClosureApply instr) {
        int cls = GetRegister(instr.closure.name);
        String rc = "r" + cls;
        int popSize = 0;

        text.append("\n@starting apply cls");

        text.append("\n\tLDR r2, [").append(rc).append("]");
        text.append("\n\tSUB r2, r2, #1");  // r2 <- (size - 1)
        text.append("\n\tADD r1, ").append(rc).append(", #8");
        text.append("\n\tADD r1, r1, r2, LSL #2"); // r1 <- address of the last arg (rc + r2*4 + 8)
        text.append("\nfor_apply_cls : ");
        text.append("\n\tCMP r2, #0");
        text.append("\n\tBLT end_for_apply_cls");
        text.append("\n\tLDR r0, [r1]");
        text.append("\n\tSUB sp, sp, #4");
        text.append("\n\tSTR r0, [sp]");
        text.append("\n\tSUB r2, r2, #1");
        text.append("\n\tBAL for_apply_cls");

        text.append("\nend_for_apply_cls : \n");



        List<Operand> args = instr.args;
        for (int i = args.size() - 1; i >= 0; i--) {
            text.append("\n\t@ push argument ").append(i);
            text.append("\n\tSUB sp, #4");
            ExecAssignment("r0", args.get(i));
            text.append("\n\tSTR r0, [sp]");
            popSize += 4;
        }

        text.append("\n\t@ call, free stack and set the return value");
        text.append("\n\tBX ").append(rc); //TODO TEST BX [rc, #4] in assembly
        // text.append("\n\tBX [").append(rc).append(", #4"); //TODO TEST BX [rc, #4] in assembly

        // add popSize (size of args) + size of free args from the closure (in [rc])
        text.append("\n\tLDR r2, [").append(rc).append("]");
        text.append("\n\tADD r2, r2, #").append(Integer.toString(popSize));
        text.append("\n\tADD sp, r2"); // erase free_args + args from the stack

        if(instr.ret != null){
            ExecAssignment(instr.ret, "r11");
        }
        text.append("\n\t@ end apply cls");

    }



    private void GenAssign(@Nonnull Assign i) {
        ExecAssignment(i.variable, i.opr); // r0 <- op
    }

    private void GenAddInt(@Nonnull AddI i) {
        if(i.variable.name.equals("V5")){
            System.out.println("here!!");
        }
        GenBinaryOperation("ADD", i.variable, i.opr1, i.opr2);
    }

    private void GenSubInt(@Nonnull SubI i) {
        GenBinaryOperation("SUB", i.variable, i.opr1, i.opr2);
    }

    private void GenJump(@Nonnull Jump i) {
        text.append("\n\tBAL ").append(i.label.name);
    }

    private void GenBranch(@Nonnull Branch i) {
        ExecAssignment("r1", i.opr1); // r1 <- opr1
        ExecAssignment("r2", i.opr2); // r2 <- opr2

        String memo = i.lessOrEqual ? "\n\tBLE " : "\n\tBEQ ";

        text.append("\n\tCMP r1, r2");
        text.append(memo).append(i.if_True.name);
        text.append("\n\tBAL ").append(i.if_False.name);
    }

    private void GenRet(@Nonnull Ret i) {
        if (i.opr == null) return;
        if (i.opr instanceof Var) {
            if (((Var) i.opr).name.equals(RETURN_KEYWORD)) {
                return;
            }
        }
        ExecAssignment("r11", i.opr);
    }

    public void WrtAssemblyCode(@Nonnull PrintStream out) {
        // function check_heap :
        text.append("\ncheck_heap :");
        text.append("\n\tLDR r0, =limit");
        text.append("\n\tLDR r0, [r0]");
        text.append("\n\tCMP r1, r0");
        text.append("\n\tBGT error_in_heap");
        text.append("\n\tMOV PC, LR");
        text.append("\nerror_in_heap :");
        text.append("\n\tLDR r0, =error_message");
        text.append("\n\tBL min_caml_print_string");
        text.append("\n\tBL min_caml_exit\n");
        //TODO MERGE CHECK_HEAP INTO MALLOC
        // function malloc (r0) :
        text.append("\nmalloc :");
        text.append("\n\tLDR r1, =head");
        text.append("\n\tLDR r2, [r1]");
        text.append("\n\tADD r1, r2, r0, LSL #2");
        text.append("\n\tBL check_heap");
        text.append("\n\tLDR r0, =head");
        text.append("\n\tSTR r1, [r0]");
        text.append("\n\tMOV r0, r1\n");

        //prints everything contained in data and text in the output_file
        out.println(data.toString());
        out.println(text.toString());
    }

}