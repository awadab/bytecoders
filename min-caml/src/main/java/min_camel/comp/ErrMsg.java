package min_camel.comp;

import ldf.java_cup.runtime.LocOfEntity;

import javax.annotation.Nonnull;

public class ErrMsg implements Comparable<ErrMsg> {

    public Type type;
    public LocOfEntity loc;
    public String message;

    public Exception ex;

    public enum Type {
        INFO,
        WARN,
        ERROR
    }

    @Override
    public int compareTo(@Nonnull ErrMsg that) {
        int off1, off2;
        if (this.loc == null || that.loc == null) return 0;
        off1 = this.loc.getOffsetLeft();
        off2 = that.loc.getOffsetLeft();
        if (off1 != off2) return off1 - off2;
        off1 = this.loc.getOffsetRight();
        off2 = that.loc.getOffsetRight();
        return off1 - off2;
    }


}
