package min_camel.comp;

import ldf.java_cup.runtime.*;
import min_camel.ast.AstExp;
import min_camel.type.Checker;
import min_camel.type.FloatType;
import min_camel.type.FuncType;
import min_camel.type.IntType;
import min_camel.type.UnitType;
import min_camel.type.Type;
import min_camel.visit.AlphaConversion;
import min_camel.visit.BetaReduction;
import min_camel.visit.PrettyPrinter;
import min_camel.type.*;
import min_camel.visit.*;
import min_camel.gen.Lexer;
import min_camel.gen.Parser;
import min_camel.helpers.Pair;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;
import min_camel.ir.CodeGenerator;
import min_camel.ir.Function;
import min_camel.ir.Instructions.Instr;
import min_camel.ir.Instructions.Label;
import min_camel.knorm.KnormNode;
import min_camel.knorm.Program;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.*;

public class Compiler {
    private Reader inputReader;
    private AstExp astParsed;
    private AstExp astTransformed;
    private KnormNode kNormalized;
    private Program closureConv;
    private final Set<ErrMsg> messageLog = new TreeSet<>();
    private boolean beginParse, successParse, perrors;
    private boolean beginTyping, successTyping;
    private boolean freeVarsBegun, freeVarsSuccessful;
    private List<Function> funDefs;



    public static final Map<String, Type> PREDEFS;
    static {
        Map<String, Type> predefs  = new LinkedHashMap<>();

        Type UNIT = UnitType.INSTANCE;
        Type INT = IntType.INSTANCE;
        Type FLOAT = FloatType.INSTANCE;

        predefs.put("print_newline", new FuncType(UNIT, UNIT));
        predefs.put("print_int", new FuncType(INT, UNIT));

        Type floatFun = new FuncType(FLOAT, FLOAT);

        predefs.put("abs_float", floatFun);
        predefs.put("sqrt", floatFun);
        predefs.put("sin", floatFun);
        predefs.put("cos", floatFun);

        predefs.put("float_of_int", new FuncType(INT, FLOAT));
        predefs.put("int_of_float", new FuncType(FLOAT, INT));
        predefs.put("truncate", new FuncType(FLOAT, INT));

        PREDEFS = Collections.unmodifiableMap(predefs);
    }

    private Checker tChecker;

    public Compiler(@Nonnull Reader in) {
        inputReader = in;
    }

    public void error(
            @Nullable LocOfEntity loc,
            @Nonnull String msg,
            @Nullable Exception e
    ) {
        ErrMsg m = new ErrMsg();
        m.type = ErrMsg.Type.ERROR;
        m.loc = loc;
        m.message = msg;
        m.ex = e;
        messageLog.add(m);
    }

    public void error(LocOfEntity loc, @Nonnull String msg) {
        error(loc, msg, null);
    }

    private void error(String s, Exception e) {
        error(null, s, e);
    }

    public void error(@Nonnull String s) {
        error(null, s, null);
    }

    public void warn(@Nonnull String msg) {
        ErrMsg m = new ErrMsg();
        m.type = ErrMsg.Type.WARN;
        m.message = msg;
        messageLog.add(m);
    }


    public void parseError(Symbol s) {
        perrors = true;
        // if (s.getSymbolName() == "EOF"){
        //     error(s, "Type mismatch.");
        // }
        error(s, "Syntax error. Unexpected " + s.getSymbolName() + ".");
    }

    public boolean parseCode() {
        Parser p;

        if (beginParse) return successParse;
        beginParse = true;

        try {
            TokenFactory tf = new MyTokenFactory();
            p = new Parser(this, tf, new Lexer(inputReader, tf));
            astParsed = (AstExp) p.parse().value;
        } catch (Exception e) {
            error("An exception has occurred while parsing.", e);
            return false;
        }

        if (astParsed == null && messageLog.size() != 0) {
            error("Parser returned `null` instead of an AST.");
            return false;
        }

        astTransformed = astParsed;
        return successParse = !perrors;
    }

    public AstExp getParseTree() {
        return parseCode() ? astParsed : null;
    }


    public boolean typeCheck() {
        if (beginTyping) return successTyping;
        beginTyping = true;

        tChecker = new Checker(astParsed, PREDEFS);
        successTyping = tChecker.wellTyped();

        if (!successTyping) {
            for (Pair<Type, Type> e : tChecker.getErrors()) {
                ErrMsg msg = new ErrMsg();
                msg.type = ErrMsg.Type.ERROR;
                msg.message = "Type mismatch: " + e.left + " = " + e.right;
                messageLog.add(msg);
            }
        }

        return successTyping;
    }

    public boolean freeCheck() {
        if (freeVarsBegun) return freeVarsSuccessful;
        freeVarsBegun = true;

        FreeVariables fvv = FreeVariables.compute(astParsed, PREDEFS.keySet());

        Set<SymRef> freeVars = fvv.getFreeVariables();

        for (SymRef i : freeVars) {
            error(i.getSymbol(), "Unknown symbol: " + i.id + ".");
        }

        return freeVarsSuccessful = freeVars.isEmpty();
    }

    public void outputAST(PrintStream out) {
        astParsed.accept(new PrettyPrinter(out));
        out.print("\n");
    }

    public void outputastTransformed(PrintStream out) {
        astTransformed.accept(new PrettyPrinter(out));
        out.print("\n");
    }

    private boolean checkDuplicateDecl() {
        boolean passed = true;
        List<Collection<SymDef>> dup = DuplicateDeclaration.Execute(astParsed);
        for (Collection<SymDef> incident : dup) {
            for (SymDef sym : incident) {
                error(sym.getSymbol(),
                        "Duplicate symbol declaration: " + sym.id
                );
                passed = false;
            }
        }
        return passed;
    }

    private void reportUnusedVars() {
        Set<String> unusedVars = UnusedVar.compute(astParsed);
        for (String unusedVar : unusedVars) {
            warn("Unused variable: " + unusedVar);
        }
    }
 

    private void transformAlphaConversion() {
        astTransformed = AlphaConversion.transform(astTransformed);
    }

    private void transformBetaReduction() {
        astTransformed = BetaReduction.compute(astTransformed);
    }

    private void transformConstantFolding() {
        astTransformed = ConstantFolding.compute(astTransformed);
    }
    private void transformInlining() {
        astTransformed = Inlining.compute(astTransformed);
    }

    private void transformElimination() {
        astTransformed = UnusedElim.Execute(astTransformed);
    }

    // generate intermediate code
    public boolean IR_gen() {
        try {
            funDefs = CodeGenerator.Compile(
                    PREDEFS, tChecker, closureConv, "_main"
            );
        } catch (RuntimeException e) {
            error("An exception has occurred while generating the IR.", e);
            return false;
        }
        return true;
    }
// print IR
    public void IR_out(PrintStream out) {
        for(Function fd : funDefs){
            out.println("# Function: " + fd.name.name);
            out.println("# Closure: " + fd.free);
            out.println("# Arguments: " + fd.args);
            out.println("# Locals: " + fd.locals);
            out.println(fd.name.toString());
            for (Instr i : fd.body) {
                if (!(i instanceof Label)) out.print('\t');
                out.println(i.toString());
            }
            out.println();
        }
    }
    public boolean transformCode() {

        reportUnusedVars();

        if (!checkDuplicateDecl()) return false;

        transformAlphaConversion();
        transformBetaReduction();
        int i = 0;
        while(i < 4){
            i++;
            transformInlining();
            transformConstantFolding();
            transformElimination();
        }
        return true;
    }

    public void printErrors(PrintStream err) {
        StringBuilder sb = new StringBuilder(1000);
        for (ErrMsg msg : messageLog) {
            sb.setLength(0);
            sb.append('[').append(msg.type).append("]");

            LocOfEntity loc = msg.loc;
            if (loc != null) {
                sb.append(" (");
                sb.append(loc.getLLeft());
                sb.append(':');
                sb.append(loc.getColLeft());
                sb.append(")-(");
                sb.append(loc.getLRight());
                sb.append(':');
                sb.append(loc.getColRight());
                sb.append("):");
            }

            sb.append(' ');
            sb.append(msg.message);
            err.println(sb.toString());

            if (msg.ex != null) {
                msg.ex.printStackTrace(err);
            }
        }
    }

    public boolean performKNormalization() {
        try {
            kNormalized = KNormalize.Execute(astTransformed);
        } catch (RuntimeException e) {
            error("An exception has occurred while K-normalizing.", e);
            return false;
        }
        return true;
    }

    public boolean performClosureConversion() {
        try {
            closureConv = ClosureConversion.execute(kNormalized, PREDEFS.keySet());
        } catch (RuntimeException e) {
            error("An exception has occurred during closure-conversion.", e);
            return false;
        }
        return true;
    }
    
        //from 3-address code to Assembly code
        public boolean outputAssembly(PrintStream out) {
            try {
                AssemblyGenerator ag = new AssemblyGenerator();
                ag.GenAssemblyCode(funDefs);
                ag.WrtAssemblyCode(out);
            }catch (RuntimeException e) {
                error("An exception has occurred while generating assembly.", e);
                return false;
            }
            return true;
        }

}