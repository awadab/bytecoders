package min_camel;

import min_camel.comp.Compiler;

import java.io.*;

public class App {


    private Reader in;
    private PrintStream out;
    private Compiler comp;

    private static final int EXIT_ERROR = 1;
    private static final int EXIT_SUCCESS = 0;

    private int optionsSelected;
    private String outputFileName;
    private String inputFileName;
    private boolean onlyTypeCheck;
    private boolean myopt;
    private boolean printASML;
    private boolean helpDisp;
    private boolean versionDisp;
    private boolean onlyParse;
    private boolean printAfterTransform;
    private boolean ASTAfterParse;
    private boolean printIR;


    static public void main(String argv[]) {
        App main = new App();
        if (!main.CMD(argv)) {
            System.exit(EXIT_ERROR);
            return;
        }
        if (main.execute()) {
            System.exit(EXIT_SUCCESS);
        } else {
            System.exit(EXIT_ERROR);
        }
    }

    private boolean CMD(String argv[]) {
        int i = 0, n = argv.length;

        if (n == 0) {
            Help();
            return false;
        }

        while (i < n) {
            String arg = argv[i++];
            if (arg.equals("-h")) {
                optionsSelected++;
                helpDisp = true;
                continue;
            }
            if (arg.equals("-v")) {
                optionsSelected++;
                versionDisp = true;
                continue;
            }
            if (arg.equals("-asml")) {
                optionsSelected++;
                printASML = true;
                continue;
            }
            
            if (arg.equals("-my-opt")) {
                optionsSelected++;
                myopt = true;
                continue;
            }
            if (arg.equals("-p")) {
                optionsSelected++;
                onlyParse = true;
                continue;
            }
            if (arg.equals("-t")) {
                optionsSelected++;
                onlyTypeCheck = true;
                continue;
            }
            // test after transformations
            if (arg.equals("-test1")) {
                optionsSelected++;
                printAfterTransform = true;
                continue;
            }
            // test after parsing
            if (arg.equals("-test0")) {
                optionsSelected++;
                ASTAfterParse = true;
                continue;
            }
            // test IR output
            if (arg.equals("-test2")) {
                optionsSelected++;
                printIR = true;
                continue;
            }
            
            
            if (arg.equals("-o")) {
                if (i == n) {
                    System.err.println("Missing filename after `-o`");
                    return false;
                }
                if (outputFileName != null) {
                    System.err.println("Duplicate `-o` argument");
                    return false;
                }

                outputFileName = argv[i++];
                continue;
            }
            if (inputFileName == null) {
                inputFileName = arg;
                continue;
            }
            System.err.println("Unexpected argument " + i + ": \n" + arg);
            return false;
        }

        if (optionsSelected > 1) {
            System.err.println("Too many actions (you must pick one).");
            return false;
        }


        if (inputFileName == null) {
            in = new BufferedReader(new InputStreamReader(System.in));
        } else {
            try {
                in = new FileReader(inputFileName);
            } catch (FileNotFoundException e) {
                System.err.println("File inaccessible: " + e.getMessage());
                return false;
            }
        }

        if (outputFileName == null) {
            out = System.out;
        } else {
            try {
                out = new PrintStream(new FileOutputStream(outputFileName));
            } catch (FileNotFoundException e) {
                System.err.println("File inaccessible: " + e.getMessage());
                return false;
            }
        }

        return true;
    }

    private boolean execute() {
        if (helpDisp) {
            Help();
            return true;
        }
        if (versionDisp) {
            printVersion();
            return true;
        }
        // if (printASML){
        //     System.out.println("Not Implemented Yet");
        //     return true;
        // }
        // if (myopt){
        //     System.out.println("Not Implemented Yet");
        //     return true;
        // }
        comp = new Compiler(in);
        boolean res = execution();
        comp.printErrors(System.err);
        return res;
    }

    private boolean execution() {
        // Parsing
        boolean GoodParse = comp.parseCode();
        if (!GoodParse) return false;
        if (onlyParse) return true;
        if (ASTAfterParse) {
            comp.outputAST(out);
            out.println();
            return GoodParse;
        }
        // Typechecking
        if (!comp.freeCheck()) return false;
        if (!comp.typeCheck()) return false;
        if (onlyTypeCheck) return true;
        
        // Transformation frontend
        if (!comp.transformCode()) return false;
        if (printAfterTransform) {
            comp.outputastTransformed(out);
            out.println();
            return true;
        }
        
        // Compilation
        if (!comp.performKNormalization()) return false;
        if (!comp.performClosureConversion()) return false;

        // generate IR
        if (!comp.IR_gen()) return false;
        if (printIR || printASML) {
            comp.IR_out(out);
            return true;
        }else {
            comp.outputAssembly(out);
            return true;
        }
    }

 

    private static void Help() {
        System.out.println("Usage: java -jar mini-camel.jar <action> [-o <output>] [<input>]");
        System.out.println("Actions:");
        System.out.println("\t-h\tDisplays helper message");
        System.out.println("\t-v\tDisplays the version of the compiler");
        System.out.println("\t-p\tOnly parse the input");
        System.out.println("\t-t\tOnly perform type checking");
        System.out.println("\t-asml\tOutput of ASML");
        System.out.println("\t-my-opt\tAdd personal options");
        System.out.println("\t-test0\tOutput of AST after parsing");
        System.out.println("\t-test1\tOutput of AST  after transformations");
        System.out.println("\t-test2\tOutput of Intermediate representation");
        System.out.println("\t -o <output> Specifies an output file; default is stdout.");
        System.out.println("\t<input>");
        System.out.println("\t  \tOptional. Specifies the input file; default is stdin.");
    }

    private static void printVersion() {
        Package p = App.class.getPackage();
        String version1 = p.getSpecificationVersion();
        String version2 = p.getImplementationVersion();
        if (version1 != null) {
            System.out.println(version1);
            return;
        }
        if (version2 != null) {
            System.out.println(version2);
            return;
        }
        System.out.println("unknown version");
    }
}

