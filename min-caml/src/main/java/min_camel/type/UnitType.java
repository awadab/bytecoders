package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

// Unit/void data type
@Immutable
public final class UnitType extends Type {

    public static final UnitType INSTANCE = new UnitType();

    // singleton class hence the use of INSTANCE
    private UnitType() {}

    @Nonnull
    public String toString() {
        return "()";
    }

    @Nonnull
    public Kind getKind() {
        return Kind.UNIT;
    }
}
