package min_camel.type;

import javax.annotation.Nonnull;

public abstract class Type {
    private static int x = 0;

    public enum Kind {
        VARIABLE,
        BOOL,
        ARRAY,
        FUNCTION,
        UNIT,
        INTEGER,
        FLOAT,
        TUPLE
    }

    @Nonnull
    public static Type generate() {
        return new VarType("t" + x++);
    }

    @Nonnull
    public abstract String toString();

    @Nonnull
    public abstract Kind getKind();
}
