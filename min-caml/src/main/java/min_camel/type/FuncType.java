package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

// Function with return and an argument
@Immutable
public final class FuncType extends Type {
    @Nonnull
    public final Type arg;

    @Nonnull
    public final Type ret;

    public FuncType(Type arg, Type ret) {
        this.arg = arg;
        this.ret = ret;
    }

    public FuncType() {
        this(Type.generate(), Type.generate());
    }

    @Nonnull
    public String toString() {
        return "(" + arg + " -> " + ret + ")";
    }

    @Nonnull
    public Kind getKind() {
        return Kind.FUNCTION;
    }
}
