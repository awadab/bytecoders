/**
 * Deals with types and type checking.
 */
@javax.annotation.ParametersAreNonnullByDefault
package min_camel.type;