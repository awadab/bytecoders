package min_camel.type;

import min_camel.ast.*;
import min_camel.helpers.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class EqtGenerator {

    static final Type UNIT = UnitType.INSTANCE;
    static final Type BOOL = BoolType.INSTANCE;
    static final Type INT = IntType.INSTANCE;
    static final Type FLOAT = FloatType.INSTANCE;

    public List<Pair<Type, Type>> GenerateEquations(
            final AstExp exp,
            final Map<String, Type> predefs
    ) {
        List<Pair<Type, Type>> out = new ArrayList<>();
        SymbolTable<Type> env = new SymbolTable<>();
        env.push();
        env.putAll(predefs);
        EqtGenerate(out, env, exp, UNIT);
        return out;
    }
    // Algorithm GenEquations: 
    // Input: (out,Expr, Environment, Type) 
    // Output: A list of equations (Type, Type)
    private void EqtGenerate(
            final List<Pair<Type, Type>> out,
            final SymbolTable<Type> environment,
            final AstExp expression,
            final Type type
    ) {
        expression.accept(new Visitor() {

            private void tEqual(Type t1, Type t2) {
                out.add(new Pair<>(t1, t2));
            }

            private void typeofExp(AstExp expr, Type t) {
                if (expr == expression) {
                    throw new RuntimeException("Possibly infinite recursion");
                }
                EqtGenerate(out, environment, expr, t);
            }

            public void visit(AstErr e) {
                // no equations are generated for an erroneous AST node
            }

            public void visit(AstUnit e) {
                tEqual(type, UNIT);
            }

            public void visit(AstBool e) {
                tEqual(type, BOOL);
            }

            public void visit(AstInt e) {
                tEqual(type, INT);
            }

            public void visit(AstAdd e) {
                tEqual(type, INT);
                typeofExp(e.e1, INT);
                typeofExp(e.e2, INT);
            }

            public void visit(AstFAdd e) {
                tEqual(type, FLOAT);
                typeofExp(e.e1, FLOAT);
                typeofExp(e.e2, FLOAT);
            }

            public void visit(AstSub e) {
                tEqual(type, INT);
                typeofExp(e.e1, INT);
                typeofExp(e.e2, INT);
            }

            public void visit(AstFSub e) {
                tEqual(type, FLOAT);
                typeofExp(e.e1, FLOAT);
                typeofExp(e.e2, FLOAT);
            }

            public void visit(AstNot e) {
                tEqual(type, BOOL);
                typeofExp(e.e, BOOL);
            }

            public void visit(AstFloat e) {
                tEqual(type, FLOAT);
            }

            public void visit(AstNeg e) {
                tEqual(type, INT);
                typeofExp(e.e, INT);
            }

            public void visit(AstFNeg e) {
                tEqual(type, FLOAT);
                typeofExp(e.e, FLOAT);
            }

            
            public void visit(AstFMul e) {
                tEqual(type, FLOAT);
                typeofExp(e.e1, FLOAT);
                typeofExp(e.e2, FLOAT);
            }

            public void visit(AstFDiv e) {
                tEqual(type, FLOAT);
                typeofExp(e.e1, FLOAT);
                typeofExp(e.e2, FLOAT);
            }

            public void visit(AstEq e) {
                Type t = Type.generate();
                tEqual(type, BOOL);
                typeofExp(e.e1, t);
                typeofExp(e.e2, t);
            }

            public void visit(AstLE e) {
                Type t = Type.generate();
                tEqual(type, BOOL);
                typeofExp(e.e1, t);
                typeofExp(e.e2, t);
            }

            public void visit(AstIf e) {
                typeofExp(e.eCond, BOOL);
                typeofExp(e.eThen, type);
                typeofExp(e.eElse, type);
            }

            public void visit(AstLet e) {
                typeofExp(e.initializer, e.decl.type);
                environment.push();
                {
                    environment.put(e.decl.id, e.decl.type);
                    typeofExp(e.ret, type);
                }
                environment.pop();
            }

            public void visit(AstLetRec e) {
                environment.push();
                {
                    environment.put(e.fd.decl.id, e.fd.decl.type);
                    typeofExp(e.fd, e.fd.decl.type);
                    typeofExp(e.ret, type);
                }
                environment.pop();
            }

            public void visit(SymRef e) {
                tEqual(type, environment.get(e.id));
            }

            

            public void visit(AstFunDef e) {

                environment.push();
                {
                    for (SymDef arg : e.args) {
                        environment.put(arg.id, arg.type);
                    }

                    //tEqual(type, e.functionType);
                    typeofExp(e.body, e.returnType);
                }
                environment.pop();
            }

            private void curry(
                    Type CalleeT,
                    Iterator<AstExp> args,
                    Type RetT
            ) {
                if (!args.hasNext()) {
                    tEqual(CalleeT, RetT);
                    return;
                }
                FuncType functionType = new FuncType();
                tEqual(CalleeT, functionType);

                AstExp arg = args.next();
                typeofExp(arg, functionType.arg);

                curry(functionType.ret, args, RetT);
            }

            public void visit(AstApp e) {
                Type calleeType = Type.generate();
                typeofExp(e.e, calleeType);
                curry(calleeType, e.es.iterator(), type);
            }

            public void visit(AstTuple e) {
                List<Type> items = new ArrayList<>(e.es.size());
                for (AstExp item : e.es) {
                    Type t = Type.generate();
                    typeofExp(item, t);
                    items.add(t);
                }
                tEqual(type, new TupleType(items));
            }

            public void visit(AstLetTuple e) {
                typeofExp(e.initializer, new TupleType(e.getIdentifierTypes()));

                environment.push();
                {
                    for (SymDef sym : e.ids) {
                        environment.put(sym.id, sym.type);
                    }

                    typeofExp(e.ret, type);
                }
                environment.pop();
            }

            public void visit(AstArray e) {
                Type elementType = Type.generate();
                typeofExp(e.size, INT);
                typeofExp(e.initializer, elementType);
                tEqual(type, new ArrType(elementType));
            }

            public void visit(AstGet e) {
                typeofExp(e.array, new ArrType(type));
                typeofExp(e.index, INT);
            }

            public void visit(AstPut e) {
                Type t = Type.generate();
                typeofExp(e.array, new ArrType(t));
                typeofExp(e.index, INT);
                typeofExp(e.value, t);
                tEqual(type, UNIT);
            }
        });
    }

}
