package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

// Variable where the type is not known while parsing and is checked by the type checker.
@Immutable
public final class VarType extends Type {
    public final String var;

    public VarType(@Nonnull String v) {
        this.var = v;
    }

    @Nonnull
    public String toString() {
        return var;
    }

    @Nonnull
    public Kind getKind() {
        return Kind.VARIABLE;
    }
}
