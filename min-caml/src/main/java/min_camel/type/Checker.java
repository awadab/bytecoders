package min_camel.type;

import min_camel.ast.AstExp;
import min_camel.helpers.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class Checker {

    AstExp prog;
    EqtGenerator gen;
    EqtSolver solver;
    Map<String, Type> solution;
    List<Pair<Type, Type>> in;
    List<Pair<Type, Type>> eqts;

    public Checker(AstExp pgm, Map<String, Type> predefs) {
        prog = pgm;
        gen = new EqtGenerator();
        in = gen.GenerateEquations(pgm, predefs);
        eqts = new ArrayList<>(in);
        solver = new EqtSolver(eqts);
        solution = solver.getSolution();
    }

    public AstExp getProgram() {
        return prog;
    }

    public List<Pair<Type, Type>> getEquations() {
        return eqts;
    }

    public Map<String, Type> getSolution() {
        return solution;
    }

    public Type checkType(Type t) {
        switch (t.getKind()) {
            case VARIABLE :
                Type newType = checkType(solution.get(((VarType) t).var));
                return newType != null ? newType : t;

            case ARRAY:
                Type oldElementType = ((ArrType) t).elemType;
                Type newElementType = checkType(oldElementType);
                if (oldElementType == newElementType) return t;
                return new ArrType(newElementType);

            case FUNCTION:
                FuncType fun = (FuncType) t;
                Type arg = checkType(fun.arg);
                Type ret = checkType(fun.ret);
                if (arg == fun.arg && ret == fun.ret) return t;
                return new FuncType(arg, ret);

            case TUPLE:
                boolean changed = false;
                TupleType oldTuple = (TupleType) t;
                List<Type> newItems = new ArrayList<>(oldTuple.Elements.size());
                for (Type item : oldTuple.Elements) {
                    Type newItem = checkType(item);
                    if (newItem != item) changed = true;
                    newItems.add(newItem);
                }
                return changed ? new TupleType(newItems) : oldTuple;

            default:
                return t;
        }
    }

    public boolean wellTyped() {
        return solver.ErrorList.isEmpty() && solver.circular.isEmpty();
    }

    public List<Pair<Type, Type>> getErrors() {
        return solver.ErrorList;
    }
}
