package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

// Tuple data type. Where expression i in the tuple is of type the i element of "items"

@Immutable
public final class TupleType extends Type {
    @Nonnull
    public final List<Type> Elements;

    public TupleType(List<Type> items) {
        this.Elements = Collections.unmodifiableList(items);
    }

    @Nonnull
    public String toString() {
        String s = Elements.toString();
        return "(" + s.substring(1, s.length()-1) + ")";
    }

    @Nonnull
    public Kind getKind() {
        return Kind.TUPLE;
    }
}