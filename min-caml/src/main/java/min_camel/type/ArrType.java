package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

// Array of elements of the same type.
@Immutable
public final class ArrType extends Type {
    @Nonnull
    public final Type elemType;

    public ArrType(Type t) {
        elemType = t;
    }

    @Nonnull
    public String toString() {
        return "[" + elemType + "]";
    }

    @Nonnull
    public Kind getKind() {
        return Kind.ARRAY;
    }
}