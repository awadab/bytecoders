package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

/**
 * Represents the "float" data type.
 */
@Immutable
public final class FloatType extends Type {

    public static final FloatType INSTANCE = new FloatType();

    // singleton class hence the use of INSTANCE
    private FloatType() {}

    @Nonnull
    public String toString() {
        return "float";
    }

    @Nonnull
    public Kind getKind() {
        return Kind.FLOAT;
    }
}
