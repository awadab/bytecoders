package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

// Int data type
@Immutable
public final class IntType extends Type {

    public static final IntType INSTANCE = new IntType();

    // singleton class hence the use of INSTANCE
    private IntType() {}

    @Nonnull
    public String toString() {
        return "int";
    }

    @Nonnull
    public Kind getKind() {
        return Kind.INTEGER;
    }
}
