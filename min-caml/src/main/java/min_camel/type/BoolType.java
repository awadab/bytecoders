package min_camel.type;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

// Boolean data type.
@Immutable
public final class BoolType extends Type {

    public static final BoolType INSTANCE = new BoolType();

    // singleton class hence the use of INSTANCE
    private BoolType() {}

    @Nonnull
    public String toString() {
        return "bool";
    }

    @Nonnull
    public Kind getKind() {
        return Kind.BOOL;
    }
}