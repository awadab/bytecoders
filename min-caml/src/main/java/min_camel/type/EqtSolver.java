package min_camel.type;

import java.util.*;

import min_camel.helpers.Pair;

public final class EqtSolver {

    Map<String, Type> solution;
    Deque<Pair<Type, Type>> workingSet;
    List<Pair<Type, Type>> circular;
    List<Pair<Type, Type>> ErrorList;

    public EqtSolver(List<Pair<Type, Type>> equations) {
        solution = new LinkedHashMap<>();
        ErrorList = new ArrayList<>();
        circular = new ArrayList<>();
        workingSet = new ArrayDeque<>(equations.size() * 2);
        workingSet.addAll(equations);
        solveEquations();
    }

    public Map<String, Type> getSolution() {
        return solution;
    }


//     Algorithm: Unification
//     Input : 
//     Output : List of equations
  
//   Output is a list of "solved" equations of the form:
//     (var_1, type_1), ... (var_n, type_n)

    private void solveEquations() {
        while (!workingSet.isEmpty()) {
            // remove one equation from the set
            Pair<Type, Type> currentEquation = workingSet.pop();

            // get the two types from the equality, try cast them to TVar
            Type t1 = currentEquation.left;
            Type t2 = currentEquation.right;
            VarType var1 = t1 instanceof VarType ? (VarType) t1 : null;
            VarType var2 = t2 instanceof VarType ? (VarType) t2 : null;


            if (var1 != null) {
                if (var2 != null && var1.var.equals(var2.var)) {
                    continue;
                }
                if (isRec(var1, t2)) {
                    circular.add(currentEquation);
                } else {
                    replaceAll(var1, t2);
                }
                continue;
            }

            if (var2 != null) {
                if (isRec(var2, t1)) {
                    circular.add(currentEquation);
                } else {
                    replaceAll(var2, t1);
                }
                continue;
            }

            assemble(currentEquation);
        }
        solution = Collections.unmodifiableMap(solution);
        circular = Collections.unmodifiableList(circular);
        ErrorList = Collections.unmodifiableList(ErrorList);
    }

    private void assemble(Pair<Type, Type> equation) {
        Type t1 = equation.left, t2 = equation.right;
        if (t1.getClass() != t2.getClass()) {
            ErrorList.add(equation);
            return;
        }
        switch (t1.getKind()) {
            case ARRAY:
                ArrType arr1 = (ArrType) t1;
                ArrType arr2 = (ArrType) t2;
                workingSet.add(new Pair<>(
                        arr1.elemType,
                        arr2.elemType
                ));
                return;

            case TUPLE:
            TupleType tuple1 = (TupleType) t1;
            TupleType tuple2 = (TupleType) t2;
            if (tuple1.Elements.size() != tuple2.Elements.size()) {
                ErrorList.add(equation);
                return;
            }
            for (int i = 0, n = tuple1.Elements.size(); i < n; ++i) {
                workingSet.add(new Pair<>(
                        tuple1.Elements.get(i),
                        tuple2.Elements.get(i)
                ));
            }
            return;

            case FUNCTION:
                FuncType fun1 = (FuncType) t1;
                FuncType fun2 = (FuncType) t2;
                workingSet.add(new Pair<>(fun1.arg, fun2.arg));
                workingSet.add(new Pair<>(fun1.ret, fun2.ret));
                return;
        }
    }

    private void replaceAll(VarType vart, Type with) {
        Pair<Type, Type> eq;
        int i, n;

        solution.put(vart.var, with);

        for (i = 0, n = workingSet.size(); i < n; ++i) {
            eq = workingSet.pop();
            eq = replaceInEqt(vart, with, eq);
            workingSet.addLast(eq);
        }

        replaceInList(vart, with, ErrorList);
        replaceInList(vart, with, circular);
    }

    private void replaceInList(
            VarType var, Type with,
            List<Pair<Type, Type>> list
    ) {
        int i, n = list.size();

        for (i = 0; i < n; ++i) {
            list.set(i, replaceInEqt(
                    var, with, list.get(i)
            ));
        }
    }

    private Pair<Type, Type> replaceInEqt(
            VarType var,
            Type with,
            Pair<Type, Type> eq
    ) {
        Type t1 = eq.left;
        Type t2 = eq.right;
        Type t3 = replaceInType(var, with, t1);
        Type t4 = replaceInType(var, with, t2);
        if (t1 != t3 || t2 != t4) {
            eq = new Pair<>(t3, t4);
        }
        return eq;
    }

    private Type replaceInType(VarType var, Type with, Type oldType) {
        switch (oldType.getKind()) {
            case UNIT:
            case BOOL:
            case INTEGER:
            case FLOAT:
                return oldType;

            case VARIABLE:
                if (var.var.equals(((VarType) oldType).var)) {
                    return with;
                } else {
                    return oldType;
                }

            case ARRAY:
                Type elementType = ((ArrType) oldType).elemType;
                Type newElement = replaceInType(var, with, elementType);
                if (elementType == newElement) return oldType;
                return new ArrType(newElement);

            case TUPLE:
                boolean changed = false;
                TupleType oldTuple = (TupleType) oldType;
                List<Type> newItems = new ArrayList<>(oldTuple.Elements.size());
                for (Type item : oldTuple.Elements) {
                    Type newItem = replaceInType(var, with, item);
                    if (newItem != item) changed = true;
                    newItems.add(newItem);
                }
                return changed ? new TupleType(newItems) : oldTuple;

            case FUNCTION:
                FuncType fun = (FuncType) oldType;
                Type newArg = replaceInType(var, with, fun.arg);
                Type newRet = replaceInType(var, with, fun.ret);
                if (newArg == fun.arg && newRet == fun.ret) return oldType;
                return new FuncType(newArg, newRet);
        }
        throw new IllegalArgumentException(oldType.getClass().getName());

    }

    private static boolean isRec(VarType var, Type type) {
        switch (type.getKind()) {
            case UNIT:
            case BOOL:
            case INTEGER:
            case FLOAT:
                return false;

            case VARIABLE:
                return var.var.equals(((VarType) type).var);

            case ARRAY:
                return isRec(var, ((ArrType) type).elemType);

            case TUPLE:
                for (Type item : ((TupleType) type).Elements) {
                    if (isRec(var, item)) {
                        return true;
                    }
                }
                return false;

            case FUNCTION:
                FuncType fun = (FuncType) type;
                return isRec(var, fun.arg) || isRec(var, fun.ret);
        }
        throw new IllegalArgumentException(type.getClass().getName());
    }

}
