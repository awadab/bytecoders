package min_camel.knorm;
import min_camel.type.IntType;
import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class KnormInt extends KnormNode {

    public static final KnormInt CONST_0 = new KnormInt(0);
    public static final KnormInt CONST_1 = new KnormInt(1);
    public final int IntNum;

    public KnormInt(int i) {
        this.IntNum = i;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return IntType.INSTANCE;
    }

    @Nonnull
    public String toString() {
        return Integer.toString(IntNum);
    }

}
