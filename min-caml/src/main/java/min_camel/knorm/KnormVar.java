package min_camel.knorm;
import min_camel.type.FloatType;
import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class KnormVar extends KnormNode {
    @Nonnull
    public final SymRef Variable;

    @Nonnull
    public final Type type;

    public KnormVar(SymRef v, Type t) {
        this.Variable = v;
        this.type = t;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return type;
    }

    @Nonnull
    public String toString() {
        return Variable.id;
    }

}
