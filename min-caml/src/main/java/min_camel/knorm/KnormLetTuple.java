package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class KnormLetTuple extends KnormNode {
    @Nonnull
    public final List<SymDef> Ids;

    @Nonnull
    public final SymRef init;

    @Nonnull
    public final KnormNode ret;

    public KnormLetTuple(List<SymDef> ids, SymRef i, KnormNode ret) {
        this.Ids = Collections.unmodifiableList(ids);
        this.init = i;
        this.ret = ret;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return ret.getDataType();
    }

    @Nonnull
    public String toString() {
        return String.format(
                "let %s = %s in (%s)",
                Ids, init.id, ret.toString()
        );
    }

}
