package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;

/*
 * After executing the CLosure Make, we can call the function object with this class.
 */
@Immutable
public final class KnormCallFunc extends KnormNode {
    @Nonnull
    public final SymRef FuncObject;

    @Nonnull
    public final List<SymRef> BoundedArgs;

    public KnormCallFunc(SymRef Fobj, List<SymRef> args) {
        this.FuncObject = Fobj;
        this.BoundedArgs = args;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return null; // fixme after closure conversion
    }

    @Nonnull
    public String toString() {
        return String.format(
                "ApplyClosure(%s, %s)",
                FuncObject.id,
                BoundedArgs.toString());
    }
}
