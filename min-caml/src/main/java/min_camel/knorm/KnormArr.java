package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;
import min_camel.type.ArrType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class KnormArr extends KnormNode {
    @Nonnull
    public final SymRef size;

    @Nonnull
    public final SymRef initValue;

    @Nonnull
    public final Type elemType;

    @Nonnull
    public final Type arrType;

    public KnormArr(SymRef s, SymRef init, Type elemT) {
        this.size = s;
        this.initValue = init;
        this.elemType = elemT;
        this.arrType = new ArrType(elemT);
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return arrType;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "Array.Create[%s](%s)",
                size, initValue);
    }
}
