package min_camel.knorm;
import min_camel.type.UnitType;
import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class KnormPut extends KnormNode {
    @Nonnull
    public final SymRef arr;

    @Nonnull
    public final SymRef index;

    @Nonnull
    public final SymRef value;

    public KnormPut(SymRef arr, SymRef i, SymRef val) {
        this.arr = arr;
        this.index = i;
        this.value = val;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return UnitType.INSTANCE;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%s.(%s) <- %s",
                arr, index, value
        );
    }

}
