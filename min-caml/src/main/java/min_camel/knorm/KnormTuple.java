package min_camel.knorm;
import min_camel.type.FloatType;
import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public final class KnormTuple extends KnormNode {
    @Nonnull
    public final List<SymRef> Elements;

    @Nonnull
    public final Type type;

    public KnormTuple(List<SymRef> elem, Type t) {
        this.Elements = Collections.unmodifiableList(elem);
        this.type = t;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return type;
    }

    @Nonnull
    public String toString() {
        return Elements.toString();
    }
}
