/**
 * Implements K-normal form of the source code, but also the closure-converted
 * form (which only adds a few classes beyond what is normally used in
 * K-normalized code).
 */
@javax.annotation.ParametersAreNonnullByDefault
package min_camel.knorm;