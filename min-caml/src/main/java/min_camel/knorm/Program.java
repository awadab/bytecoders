package min_camel.knorm;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collections;
import java.util.Map;

@ParametersAreNonnullByDefault
public final class Program {
    @Nonnull
    public final Map<String, KnormFuncDefinition> topLevel;

    @Nonnull
    public final KnormNode mainBody;

    public Program(Map<String, KnormFuncDefinition> topLvl, KnormNode mainBody) {
        this.topLevel = Collections.unmodifiableMap(topLvl);
        this.mainBody = mainBody;
    }
}
