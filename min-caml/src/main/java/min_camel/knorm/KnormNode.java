package min_camel.knorm;
import min_camel.type.FloatType;
import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class KnormNode {

    public abstract void accept(KnormVisitor v);

    public abstract <T> T accept(KnormVisitor1<T> v);

    public abstract <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u);

    @Nonnull
    public abstract Type getDataType();

    @Nonnull
    public abstract String toString();
}
