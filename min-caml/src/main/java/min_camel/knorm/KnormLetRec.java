package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class KnormLetRec extends KnormNode {
    @Nonnull
    public final KnormFuncDefinition FuncDef;

    @Nonnull
    public final KnormNode ret;

    public KnormLetRec(KnormFuncDefinition fd, KnormNode ret) {
        this.FuncDef = fd;
        this.ret = ret;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return FuncDef.FuncName.type;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "let rec %s %s = (%s) in %s",
                FuncDef.FuncName.id, FuncDef.FuncArgs,
                FuncDef.FuncBody, ret.toString()
        );
    }

}
