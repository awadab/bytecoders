package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class KnormLet extends KnormNode {
    @Nonnull
    public final SymDef Id;

    @Nonnull
    public final KnormNode init;

    @Nonnull
    public final KnormNode ret;

    public KnormLet(SymDef id, KnormNode i, KnormNode ret) {
        this.Id = id;
        this.init = i;
        this.ret = ret;
    }


    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return Id.type;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "(let %s = %s in %s)",
                Id.id, init, ret
        );
    }

}
