package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class KnormApplication extends KnormNode {
    @Nonnull
    public final SymRef symbRef;

    @Nonnull
    public final List<SymRef> args;

    @Nonnull
    public final Type returnedType;

    public KnormApplication(SymRef f, List<SymRef> args, Type returnedType) {
        this.symbRef = f;
        this.args = Collections.unmodifiableList(args);
        this.returnedType = returnedType;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return returnedType;
    }

    @Nonnull
    public String toString() {
        return symbRef.id + args;
    }
}
