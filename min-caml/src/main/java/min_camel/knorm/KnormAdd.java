package min_camel.knorm;

import min_camel.type.Type;
import min_camel.type.IntType;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class KnormAdd extends KnormNode {
    @Nonnull
    public final SymRef opr1, opr2;

    public KnormAdd(SymRef opr1, SymRef opr2) {
        this.opr1 = opr1;
        this.opr2 = opr2;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return IntType.INSTANCE;
    }

    @Nonnull
    public String toString() {
        return opr1.id + " + " + opr2.id;
    }
}
