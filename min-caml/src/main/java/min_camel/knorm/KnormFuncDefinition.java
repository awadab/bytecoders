package min_camel.knorm;

import min_camel.helpers.*;
import min_camel.helpers.SymRef;
import min_camel.visit.KnormFreeVars;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.*;

@Immutable
public final class KnormFuncDefinition {
    @Nonnull
    public final SymDef FuncName;

    @Nonnull
    public final List<SymDef> FuncArgs;

    @Nonnull
    public final KnormNode FuncBody;

    @Nonnull
    public final List<SymRef> FreeVars;

    public final boolean isRec;

    public KnormFuncDefinition(
            SymDef n,
            List<SymDef> args,
            KnormNode b
    ) {
        this.FuncName = n;
        this.FuncArgs = Collections.unmodifiableList(args);
        this.FuncBody = b;

        boolean isRecursive = false;

        List<SymRef> freeV = KnormFreeVars.compute(b);
        Set<String> bound = new HashSet<>(args.size());
        for (SymDef def : args) {
            bound.add(def.id);
        }
        Iterator<SymRef> iterator = freeV.iterator();
        while (iterator.hasNext()) {
            SymRef reference = iterator.next();
            if (bound.contains(reference.id)) {
                iterator.remove();
            }
            if (reference.id.equals(n.id)) {
                isRecursive = true;
                iterator.remove();
            }
        }

        this.FreeVars = Collections.unmodifiableList(freeV);
        this.isRec = isRecursive;
    }
}
