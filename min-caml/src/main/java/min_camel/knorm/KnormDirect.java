package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

// Function call by name
@Immutable
public final class KnormDirect extends KnormNode {
    @Nonnull
    public final SymRef functionName;

    @Nonnull
    public final List<SymRef> args;

    public KnormDirect(SymRef functionName, List<SymRef> args) {
        this.functionName = functionName;
        this.args = Collections.unmodifiableList(args);
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return null; // fixme after closure conversion
    }

    @Nonnull
    public String toString() {
        return String.format(
                "ApplyDirect(%s, %s)",
                functionName.id,
                args.toString()
        );
    }
}
