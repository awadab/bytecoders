package min_camel.knorm;

import min_camel.type.Type;
import min_camel.helpers.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;

/*
 * Create a function object from the function and its free variables.
 * This object can be called from the KnormCallFunc Class.
 */
@Immutable
public final class KnormClosureMake extends KnormNode {

    @Nonnull
    public final SymRef FunName;

    @Nonnull
    public final SymDef target;

    @Nonnull
    public final List<SymRef> FreeArgs;

    @Nonnull
    public final KnormNode ret;

    public KnormClosureMake(
            SymDef t,
            SymRef name,
            List<SymRef> args,
            KnormNode ret) {
        this.target = t;
        this.FunName = name;
        this.FreeArgs = args;
        this.ret = ret;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    @Nonnull
    public Type getDataType() {
        return ret.getDataType(); // fixme: verify correctness
    }

    @Nonnull
    public String toString() {
        return String.format(
                "let %s = MakeClosure(%s, %s) in (%s)",
                target.id,
                FunName.id,
                FreeArgs.toString(),
                ret.toString());
    }
}
