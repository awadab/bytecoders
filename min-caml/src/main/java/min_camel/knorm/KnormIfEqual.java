package min_camel.knorm;

import min_camel.type.FloatType;
import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.KnormVisitor1;
import min_camel.helpers.KnormVisitor2;
import min_camel.helpers.SymRef;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class KnormIfEqual extends KnormNode {
    @Nonnull
    public final SymRef opr1, opr2;

    @Nonnull
    public final KnormNode kThen, kElse;

    public KnormIfEqual(SymRef o1, SymRef o2, KnormNode kthen, KnormNode kelse) {
        this.opr1 = o1;
        this.opr2 = o2;
        this.kThen = kthen;
        this.kElse = kelse;
    }

    public void accept(KnormVisitor v) {
        v.visit(this);
    }

    public <T> T accept(KnormVisitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(KnormVisitor2<T, U> v, @Nullable U u) {
        return v.visit(u, this);
    }

    @Nonnull
    public Type getDataType() {
        return kThen.getDataType(); // same as kElse.getDataType()
    }

    @Nonnull
    public String toString() {
        return String.format(
                "if (%s = %s) then %s else %s",
                opr1.id, opr2.id,
                kThen.toString(),
                kElse.toString());
    }

}
