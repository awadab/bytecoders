package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import min_camel.helpers.Visitor2;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;


// if e the e1 else e2
@Immutable
public final class AstIf extends AstExp {
    // condition expression for if statement of type TBool
    @Nonnull
    public final AstExp eCond;

    // then branch expression
    @Nonnull
    public final AstExp eThen;

    // else branch expression
    @Nonnull
    public final AstExp eElse;

    // both else and then branch sould be of the same type

    public AstIf(AstExp eCond, AstExp eThen, AstExp eElse) {
        this.eCond = eCond;
        this.eThen = eThen;
        this.eElse = eElse;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    @Nonnull
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("(if(");
        sb.append(eCond);
        sb.append(") then ");
        sb.append(eThen);
        sb.append(" else ");
        sb.append(eElse);
        sb.append(")");

        return sb.toString();
    }
}
