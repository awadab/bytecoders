package min_camel.ast;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import min_camel.helpers.*;
import min_camel.type.FuncType;
import min_camel.type.Type;

import java.util.Collections;
import java.util.List;

// Function definition in let rec: 
// let rec func args = e in e1
@Immutable
public final class AstFunDef extends AstExp {
    // Name and type of function which is not necessarly similar to return type (function signiature).
     
    @Nonnull
    public final SymDef decl;

    // Name and type of the args
    @Nonnull
    public final List<SymDef> args;

    // body of the function. If it contains references to other decl, the function
    // is recursive. Type of function is returnType

    @Nonnull
    public final AstExp body;

    @Nonnull
    public final Type returnType;

    @CheckForNull
    private List<String> ids_;

    @CheckForNull
    private List<Type> types_;

    public AstFunDef(Id id, List<SymDef> args, AstExp body) {

        this.returnType = Type.generate();

        Type functionType = returnType;
        for (int i = args.size() - 1; i >= 0; --i) {
            SymDef arg = args.get(i);
            functionType = new FuncType(arg.type, functionType);
        }

        this.args = Collections.unmodifiableList(args);
        this.decl = new SymDef(id, functionType);
        this.body = body;
    }

    public AstFunDef(SymDef decl, List<SymDef> args, AstExp body) {
        Type ret = decl.type; // function type
        for (SymDef arg : args) {
            FuncType tFun = (ret instanceof FuncType) ? ((FuncType) ret) : null;
            if (tFun == null || arg.type != tFun.arg) {
                throw new IllegalArgumentException("Type mismatch");
            }
            ret = tFun.ret;
        }
        this.decl = decl;
        this.args = Collections.unmodifiableList(args);
        this.body = body;
        this.returnType = ret;
    }

    @Nonnull
    public List<String> getArgumentNames() {
        if (ids_ != null) return ids_;
        synchronized (this) {
            if (ids_ != null) return ids_;
            ids_ = SymDef.Id_List(args);
        }
        return ids_;
    }

    @Nonnull
    public List<Type> getArgumentTypes() {
        if (types_ != null) return types_;
        synchronized (this) {
            if (types_!= null) return types_;
            types_ = SymDef.Types_List(args);
        }
        return types_;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }
    
    @Nonnull
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("(");
        sb.append(decl.id);

        boolean first = true;
        sb.append("(");
        for (SymDef l : args){
            if(!first){
                sb.append(", ");
            }
            first = false;
            sb.append(l.id);
        }
        sb.append(") = ");
        sb.append(body);
        sb.append(")");

        return sb.toString();
    }
}