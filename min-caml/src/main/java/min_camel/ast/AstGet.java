package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;
import min_camel.helpers.Visitor2;


/**
 * Array read operation ({@code array.(index)}).
 */
@Immutable
public final class AstGet extends AstExp {
    /**
     * The expression being interpreted as an array. Its data type is
     * expected to be {@link min_camel.type.ArrType}.
     */
    @Nonnull
    public final AstExp array;

    /**
     * The expression being interpreted as an array index. Its data type
     * is expected to be {@link min_camel.type.IntType}.
     */
    @Nonnull
    public final AstExp index;

    public AstGet(AstExp e1, AstExp e2) {
        this.array = e1;
        this.index = e2;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    @Nonnull
    public String toString(){
        return "(" + array.toString() + ".(" + index.toString() + "))";
    }
}