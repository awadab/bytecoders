package min_camel.ast;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import min_camel.helpers.SymDef;
import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;
import min_camel.type.Type;

import java.util.Collections;
import java.util.List;
import min_camel.helpers.Visitor2;

// Let expression: let (i1... iN) = e1 in e2

@Immutable
public final class AstLetTuple extends AstExp {
    // Contains name and type identifiers that are bound in the body of "let" expression
    @Nonnull
    public final List<SymDef> ids;

    // Intialize let expression. Type TTuple.
    @Nonnull
    public final AstExp initializer;

    // Ret of let expression
    @Nonnull
    public final AstExp ret;

    @CheckForNull
    private List<String> ids_;

    @CheckForNull
    private List<Type> types_;

    public AstLetTuple(List<SymDef> ids, AstExp initializer, AstExp ret) {
        this.ids = Collections.unmodifiableList(ids);
        this.initializer = initializer;
        this.ret = ret;
    }

    @Nonnull
    public List<String> getIdentifierList() {
        if (ids_ != null) return ids_;
        synchronized (this) {
            if (ids_ != null) return ids_;
            ids_ = SymDef.Id_List(ids);
        }
        return ids_;
    }

    @Nonnull
    public List<Type> getIdentifierTypes() {
        if (types_ != null) return types_;
        synchronized (this) {
            if (types_!= null) return types_;
            types_ = SymDef.Types_List(ids);
        }
        return types_;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }


    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    @Nonnull
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("(let ");

        boolean first = true;
        sb.append("(");
        for (SymDef l : ids){
            if(!first){
                sb.append(", ");
            }
            first = false;
            sb.append(l.id);
        }
        sb.append(") = ");
        sb.append(initializer);
        sb.append(" in ");
        sb.append(ret);
        sb.append(")");

        return sb.toString();
    }
}