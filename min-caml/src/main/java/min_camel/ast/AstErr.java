package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;
import min_camel.helpers.Visitor2;

// Used in case of errors when the input code is syntactically incorrect but we can still continue

public final class AstErr extends AstExp {

    public void accept(Visitor v) {
        v.visit(this);
    }


    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    @Nonnull
    public String toString() {
        return "<error>";
    }
}
