package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;
import min_camel.helpers.Visitor2;

// Not expression !e.
@Immutable
public final class AstNot extends AstExp {
    
    @Nonnull
    public final AstExp e;

    public AstNot(AstExp e) {
        this.e = e;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    @Nonnull
    public String toString(){
        return "(Not " + e.toString() + ")";
    }
}