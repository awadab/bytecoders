package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;
import min_camel.helpers.Visitor2;

// Creating Arrays.
@Immutable
public final class AstArray extends AstExp {
    
    // Size of Array.
    @Nonnull
    public final AstExp size;

    // Intialization of the elements in the created array.
    @Nonnull
    public final AstExp initializer;

    public AstArray(AstExp size, AstExp initializer) {
        this.size = size;
        this.initializer = initializer;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    @Nonnull
    public String toString(){
        return "(Array.Create " +
                size.toString() + " " +
                initializer.toString() + ")";
    }
}