package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;
import min_camel.helpers.Visitor2;

// Additition of two expressions (e1+e2).
@Immutable
public final class AstAdd extends AstExp {
    // The operands should be of type TInt.
    @Nonnull
    public final AstExp e1, e2;

    public AstAdd(AstExp e1, AstExp e2) {
        this.e1 = e1;
        this.e2 = e2;
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }


    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }
    

    @Nonnull
    public String toString() {
        return "(" + e1.toString() + " + " + e2.toString() + ")";
    }

}
