package min_camel.ast;

import min_camel.helpers.*;
import min_camel.type.UnitType;
import min_camel.type.Type;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

// let expression: let id = e1 in e2
@Immutable
public final class AstLet extends AstExp {
    
    // Name and type of the identifier
    @Nonnull
    public final SymDef decl;

    // intializer of the declared symbol
    @Nonnull
    public final AstExp initializer;

    // returned value of let. It references decl.

    @Nonnull
    public final AstExp ret;

    public AstLet(SymDef decl, AstExp initializer, AstExp ret) {
        this.decl = decl;
        this.initializer = initializer;
        this.ret = ret;
    }

    public AstLet(Id id, AstExp initializer, AstExp ret) {
        this(id, Type.generate(), initializer, ret);
    }

    private AstLet(Id id, Type t, AstExp initializer, AstExp ret) {
        this.decl = new SymDef(id, t);
        this.initializer = initializer;
        this.ret = ret;
    }

    public static AstLet semicolonSyntacticSugar(AstExp s1, AstExp s2) {
        return new AstLet(Id.gen(), UnitType.INSTANCE, s1, s2);
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }
    
    @Nonnull
    public String toString(){
        return "(let " + decl.id + " = " + initializer + " in " + ret + ")";
    }
}