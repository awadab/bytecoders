package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import min_camel.helpers.Visitor2;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;


/**
 * Array write operation (<code>array.(index)&lt;-value</code>).
 */
@Immutable
public final class AstPut extends AstExp {
    
    // Array interpreation. Type TArray.
    @Nonnull
    public final AstExp array;

    // Index of the Array of tyoe TInt.
    @Nonnull
    public final AstExp index;

    // Value of an element.
    @Nonnull
    public final AstExp value;

    public AstPut(AstExp array, AstExp index, AstExp value) {
        this.array = array;
        this.index = index;
        this.value = value;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }


    @Nonnull
    public String toString() {
        return "(" + array.toString() + ".(" + index.toString() + ") <- " + value.toString() + ")";
    }
}