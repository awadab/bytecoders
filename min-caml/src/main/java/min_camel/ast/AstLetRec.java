package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import min_camel.helpers.Visitor2;

import min_camel.helpers.SymDef;
import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;

// definition of function Let rec
@Immutable
public final class AstLetRec extends AstExp {
    
    // Declared function
    public final AstFunDef fd;

    // returned value of let rec. It references FunDef.
    public final AstExp ret;

    public AstLetRec(AstFunDef fd, AstExp ret) {
        this.fd = fd;
        this.ret = ret;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }


    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }
    
    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    @Nonnull
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("(let rec ");
        sb.append(fd.decl.id);

        boolean first = true;
        sb.append("(");
        for (SymDef l : fd.args){
            if(!first){
                sb.append(", ");
            }
            first = false;
            sb.append(l.id);
        }
        sb.append(") = ");
        sb.append(fd.body);
        sb.append(" in ");
        sb.append(ret);
        sb.append(")");

        return sb.toString();
    }
}