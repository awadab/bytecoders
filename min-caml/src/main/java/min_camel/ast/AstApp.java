package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;

import java.util.Collections;
import java.util.List;
import min_camel.helpers.Visitor2;

// Func Application: e(es1 ... esN)

@Immutable
public final class AstApp extends AstExp {
    
    // Function
    @Nonnull
    public final AstExp e;

    // Args 
    @Nonnull
    public final List<AstExp> es;

    public AstApp(AstExp e, List<AstExp> es) {
        this.e = e;
        this.es = Collections.unmodifiableList(es);
    }

    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }

    @Nonnull
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(e);
        boolean first = true;
        sb.append("(");
        for (AstExp l : es){
            if(!first){
                sb.append(", ");
            }
            first = false;
            sb.append(l);
        }
        sb.append("))");
        return sb.toString();
    }
}
