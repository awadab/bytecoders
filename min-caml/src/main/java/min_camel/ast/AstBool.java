package min_camel.ast;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import min_camel.helpers.Visitor2;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;

// Bool literal: True or False
@Immutable
public final class AstBool extends AstExp {
    
    public final boolean b;

    public static final AstBool TRUE = new AstBool(true);
    public static final AstBool FALSE = new AstBool(false);

    public AstBool(boolean b) {
        this.b = b;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }

    public <T> T accept(Visitor1<T> v) {
        return v.visit(this);
    }


    public <T, U> T accept(Visitor2<T, U> v, @Nullable U a) {
        return v.visit(a, this);
    }


    @Nonnull
    public String toString(){
        return Boolean.toString(b);
    }

    public static AstBool staticInstance(boolean b) {
        return b ? TRUE : FALSE;
    }
}
