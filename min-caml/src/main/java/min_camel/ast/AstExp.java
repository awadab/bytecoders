package min_camel.ast;

import javax.annotation.Nullable;

import min_camel.helpers.Visitor;
import min_camel.helpers.Visitor1;
import min_camel.helpers.Visitor2;


// Base class used for AST Node expressions
public abstract class AstExp extends AstNode {

    //Implement the visitor class to examine the Node
    public abstract void accept(Visitor v);

    // Implements visitor1 that also specifies the return value.
    public abstract <T> T accept(Visitor1<T> v);

    public abstract <T, U> T accept(Visitor2<T, U> v, @Nullable U a);

}
