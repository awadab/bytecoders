package min_camel.ast;

import ldf.java_cup.runtime.Symbol;

import javax.annotation.Nonnull;

public abstract class AstNode {
    private Symbol symbol;

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        if (this.symbol != null) {
            throw new IllegalStateException();
        }
        this.symbol = symbol;
    }

    //The toString is used for debugging purposes to represent the AST.
    
    @Nonnull
    public abstract String toString();

}
