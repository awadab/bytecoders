package min_camel.ir;

import min_camel.ir.Instructions.*;
import min_camel.ir.opr.*;
import min_camel.knorm.*;
import min_camel.type.Checker;
import min_camel.type.FuncType;
import min_camel.type.UnitType;
import min_camel.type.Type;
import min_camel.helpers.KnormVisitor;
import min_camel.helpers.SymDef;
import min_camel.helpers.SymRef;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.*;

@ParametersAreNonnullByDefault
public final class CodeGenerator implements KnormVisitor {
    // new stack declaration
    private Stack<Var> Assign_Stack = new Stack<>();
    // List of instructions
    private List<Instr> Code = new ArrayList<>();
    // destination variable
    private Var Destination;
    //function arguments 
    private Map<String, Var> args = new LinkedHashMap<>();
    // local variables in the function 
    private Map<String, Var> LocVars = new LinkedHashMap<>();
    // free variables
    private Map<String, Var> FreeVars = new LinkedHashMap<>();

    @Nonnull
    private Map<String, Type> GlobVars;

    // Arity is the number of arguments or operands taken by a function
    @Nonnull
    private Map<String, Integer> Arity, ArityFree;

    @CheckForNull
    private Checker typeChecker;


    //private constructor
    private CodeGenerator(
            @Nonnull Map<String, Type> glob,
            @Nonnull Map<String, Integer> ar,
            @Nonnull Map<String, Integer> freeA,
            @Nullable Checker Tchecker) {
        this.GlobVars = glob;
        this.Arity = ar;
        this.ArityFree = freeA;
        this.typeChecker = Tchecker;
    }

    // compilation method that returns a list of functions and functions contains their bodies as instructions
    public static List<Function> Compile(
            @Nullable Map<String, Type> PreDefs,
            @Nullable Checker Checker,
            @Nonnull Program prog,
            @Nonnull String Main_Name) {

        if (PreDefs == null)
        {
            PreDefs = Collections.emptyMap();
        }
        
        Map<String, KnormFuncDefinition> Tasks = new LinkedHashMap<>(prog.topLevel);
        // adding the first function defiition in tasks which is the toplevel of the program prog
        // tasks contains a map between the name of the function the KFUNCDEF where we will use the 
        // compile function on the values to get the instructions and save it in the body
        Map<String, Type> Globals = new LinkedHashMap<>(PreDefs);
        Map<String, Integer> Arity = new HashMap<>();
        Map<String, Integer> FreeArity = new HashMap<>();

        // add main function
        Tasks.put(Main_Name, new KnormFuncDefinition(
                new SymDef(Main_Name, new FuncType(UnitType.INSTANCE, UnitType.INSTANCE)),
                Collections.<SymDef>emptyList(),
                prog.mainBody));

        // System.out.println("Hello world to tasks: "+tasks);
        // System.out.println("tasks value: " +tasks.values());
        for (KnormFuncDefinition fd : Tasks.values()) {
            String id = fd.FuncName.id;
            Globals.put(id, Checker.checkType(fd.FuncName.type));
            FreeArity.put(id, fd.FreeVars.size());
            Type functionType = Checker.checkType(fd.FuncName.type);
            Arity.put(id, IsFuncVoidArgs(functionType) ? 0 : fd.FuncArgs.size());
        }

        // add all the predefs to arity with 1 argument
        for (String predef : PreDefs.keySet()) {
            Arity.put(predef, 1);
        }

        List<Function> CompiledFuncs = new ArrayList<>(prog.topLevel.size() + 1);

        for (KnormFuncDefinition FuncDef2 : Tasks.values()) {
            CodeGenerator CodeGen = new CodeGenerator(
                    Globals, Arity, FreeArity, Checker);
            CompiledFuncs.add(CodeGen.Compile(FuncDef2));
            // System.out.println(cg.compile(fd2));
        }

        // System.out.println("compile returned given list: " + compiled.get(0).body);
        // System.out.println("arity returned given list: " + arity);
        // System.out.println("free arity returned given list: " + freeArity);
        // System.out.println("globals  returned given list: " + globals);
        // System.out.println("tasks  returned given list: " + tasks);
        return CompiledFuncs;
    }

    private Function Compile(KnormFuncDefinition FDef) {
        for (SymDef arg : FDef.FuncArgs) {
            args.put(arg.id, new Var(arg.id, arg.type));
        }
        for (SymRef arg : FDef.FreeVars) {
            FreeVars.put(arg.id, new Var(arg.id, null));
        }

        Type FType = CheckType(FDef.FuncName.type);
        if (IsVoidFunc(FType)) {
            // pass the transformed ast and make the translation from knode to IR
            FDef.FuncBody.accept(this);
            // add return value for void function
            Code.add(new Ret(null));
        } else {
            Type retType = ((FuncType) FType).ret;
            PushVariable(NewLocVar(new SymDef("ret", retType)));
            // pass the transformed ast and make the translation from knode to IR
            FDef.FuncBody.accept(this);
            // add return value for a not void function
            Code.add(new Ret(Destination));
            PopVariable();
        }

        // System.out.println("\n the code from KFUNDEF is " + code + "\n");
        return new Function(
                new Label(FDef.FuncName.id),
                new ArrayList<>(FreeVars.values()),
                new ArrayList<>(args.values()),
                new ArrayList<>(LocVars.values()),
                Code);
    }

    private void PopVariable() {
        Assign_Stack.pop();
        Destination = Assign_Stack.isEmpty() ? null : Assign_Stack.peek();
    }
    
    private void PushVariable(Var v) {
        Destination = v;
        Assign_Stack.push(v);
    }

    private static boolean IsVoidFunc(@Nullable Type funType) {
        if (funType == null)
            return false;
        if (funType.getKind() != Type.Kind.FUNCTION)
            return false;
        if (((FuncType) funType).ret.getKind() != Type.Kind.UNIT)
            return false;
        return true;
    }

    private static boolean IsFuncVoidArgs(@Nullable Type funType) {
        if (funType == null)
            return false;
        if (funType.getKind() != Type.Kind.FUNCTION)
            return false;
        if (((FuncType) funType).arg.getKind() != Type.Kind.UNIT)
            return false;
        return true;
    }

    @Nonnull
    private Type CheckType(@Nullable Type t) {
        if (t == null)
            return Type.generate();
        if (typeChecker == null)
            return t;
        return typeChecker.checkType(t);
    }

    @Nonnull
    private Type CheckType(String symbol) {
        return CheckType(GlobVars.get(symbol));
    }

    private static final ConstInt CONST_0 = new ConstInt(0);
    private static final ConstInt CONST_1 = new ConstInt(1);

    private ConstInt Const(int i) {
        if (i == 0)
            return CONST_0;
        if (i == 1)
            return CONST_1;
        return new ConstInt(i);
    }

    private ConstFloat Const(float f) {
        return new ConstFloat(f);
    }

    private Var FindReference(SymRef Ref) {
        Var var1;

        var1 = LocVars.get(Ref.id);
        if (var1 != null)
            return var1;

        var1 = args.get(Ref.id);
        if (var1 != null)
            return var1;

        var1 = FreeVars.get(Ref.id);
        if (var1 != null)
            return var1;

        var1 = new Var(Ref.id, GlobVars.get(Ref.id));
        return var1;
        /*
         * if (v != null) return v;
         * 
         * throw new RuntimeException(
         * "Unknown reference " + ref.id + " found when generating IR"
         * );
         */
    }

    public void visit(KnormUnit k) {
        // do nothing
    }

    public void visit(KnormVar k) {
        Code.add(new Assign(Destination, FindReference(k.Variable)));
    }

    public void visit(KnormInt k) {
        Code.add(new Assign(Destination, Const(k.IntNum)));
    }

    public void visit(KnormFloat k) {
        Code.add(new Assign(Destination, Const(k.Fnumber)));
    }

    public void visit(KnormArr k) {
        Code.add(new ArrNew(Destination, FindReference(k.size), FindReference(k.initValue)));
    }

    public void visit(KnormTuple k) {
        int i, n = k.Elements.size();
        Code.add(new ArrNew(Destination, Const(n), null));
        for (i = 0; i < n; ++i) {
            Code.add(new ArrPut(Destination, Const(i), FindReference(k.Elements.get(i))));
        }
    }

    public void visit(KnormAdd k) {
        Code.add(new AddI(Destination, FindReference(k.opr1), FindReference(k.opr2)));
    }

    public void visit(KnormFloatAdd k) {
        Code.add(new AddF(Destination, FindReference(k.opr1), FindReference(k.opr2)));
    }

    public void visit(KnormSub k) {
        Code.add(new SubI(Destination, FindReference(k.opr1), FindReference(k.opr2)));
    }
    
    public void visit(KnormFloatSub k) {
        Code.add(new SubF(Destination, FindReference(k.opr1), FindReference(k.opr2)));
    }

    public void visit(KnormNeg k) {
        Code.add(new SubI(Destination, Const(0), FindReference(k.opr)));
    }

    public void visit(KnormFloatNeg k) {
        Code.add(new SubF(Destination, Const(0f), FindReference(k.opr)));
    }

    public void visit(KnormFloatMul k) {
        Code.add(new MultF(Destination, FindReference(k.opr1), FindReference(k.opr2)));
    }

    public void visit(KnormFloatDiv k) {
        Code.add(new DivF(Destination, FindReference(k.opr1), FindReference(k.opr2)));
    }

    public void visit(KnormLet k) {
        SymDef iden = k.Id;
        if (iden.id.startsWith("?v")) {
            k.init.accept(this);
        } else {
            PushVariable(NewLocVar(iden));
            k.init.accept(this);
            PopVariable();
        }
        k.ret.accept(this);
    }

    public void visit(KnormGet k) {
        Code.add(new ArrGet(Destination, FindReference(k.array), FindReference(k.index)));
    }

    public void visit(KnormPut k) {
        Code.add(new ArrPut(FindReference(k.arr), FindReference(k.index), FindReference(k.value)));
    }


    private Var NewLocVar(SymDef iden) {
        Var v = new Var(iden.id, iden.type);
        LocVars.put(iden.id, v);
        return v;
    }

    public void visit(KnormLetRec k) {
        // fixme ?
        throw new RuntimeException(
                "Unexpected `KLetRec`; expecting closure-converted code");
    }

    // function when we have a branch like if then else
    private void execBranch(
            boolean LorEqual,
            SymRef opr1, SymRef opr2,
            KnormNode kThen, KnormNode kElse) {
        Label lblThen = Label.gen();
        Label lblELse = Label.gen();
        Label lblEnd = Label.gen();
        Code.add(new Branch(
                LorEqual,
                FindReference(opr1), FindReference(opr2),
                lblThen, lblELse));
        Code.add(lblThen);
        kThen.accept(this);
        Code.add(new Jump(lblEnd));
        Code.add(lblELse);
        kElse.accept(this);
        Code.add(lblEnd);
    }

    public void visit(KnormIfEqual k) {
        execBranch(false, k.opr1, k.opr2, k.kThen, k.kElse);
    }

    public void visit(KnormIfLess k) {
        execBranch(true, k.opr1, k.opr2, k.kThen, k.kElse);
    }

    public void visit(KnormLetTuple k) {
        int i, n = k.Ids.size();
        Var array = FindReference(k.init);
        for (i = 0; i < n; ++i) {
            Var dest = NewLocVar(k.Ids.get(i));
            Code.add(new ArrGet(dest, array, Const(i)));
        }
        k.ret.accept(this);
    }

    public void visit(KnormCallFunc k) {
        List<Operand> args = new ArrayList<>(k.BoundedArgs.size());
        for (SymRef ref : k.BoundedArgs) {
            args.add(FindReference(ref));
        }
        // fixme: implement currying or change type system to prevent it
        // since we can't prevent it at compile-time, will crash at runtime
        Code.add(new ClosureApply(Destination, FindReference(k.FuncObject), args));
    }

    public void visit(KnormApplication k) {
        // Throw: Unexpected `KApp` in closure-converted code ?
        List<Operand> args = new ArrayList<>(k.args.size());
        for (SymRef ref : k.args) {
            args.add(FindReference(ref));
        }
        String id = k.symbRef.id;
        int arity = this.Arity.get(id);
        if (arity > args.size()) {
            throw new RuntimeException(
                    "Sorry, currying not supported for: " + id);
        }
        if (arity == 0)
            args.clear();
        Code.add(new CallFunc(Destination, id, args));
    }

    public void visit(KnormDirect k) {
        List<Operand> args = new ArrayList<>(k.args.size());
        for (SymRef ref : k.args) {
            args.add(FindReference(ref));
        }

        // Name of the function to call
        String id = k.functionName.id;
        Type t = GlobVars.get(k.functionName);
        int arity = this.Arity.get(id);
        if (arity > args.size()) {
            throw new RuntimeException(
                    "Sorry, currying not supported for: " + id);
        }
        if (arity == 0)
            args.clear();

        Code.add(new CallFunc(IsVoidFunc(t) ? null : Destination, id, args));
    }

    public void visit(min_camel.knorm.KnormClosureMake k) {
        List<Operand> args = new ArrayList<>(k.FreeArgs.size());
        for (SymRef ref : k.FreeArgs) {
            args.add(FindReference(ref));
        }
        /*
         * // fixme: is this necessary?
         * String id = k.functionName.id;
         * int arity = this.arityFree.get(id);
         * if (arity > args.size()) {
         * throw new RuntimeException(
         * "Sorry, currying not supported for: " + id
         * );
         * }
         * if (arity == 0) args.clear();
         */

        PushVariable(NewLocVar(k.target));
        Code.add(new min_camel.ir.Instructions.ClosureMake(Destination, new Label(k.FunName.id), args));
        PopVariable();
        k.ret.accept(this);
    }
}
