package min_camel.ir.Instructions;

import min_camel.ir.opr.*;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class ArrGet implements Instr {
    @Nonnull
    public final Var out, arr;

    @Nonnull
    public final Operand index;

    public ArrGet(Var out, Var array, Operand index) {
        this.out = out;
        this.arr = array;
        this.index = index;
    }

    @Nonnull
    public Type getInstrType() {
        return Type.ARRAY_GET;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%s := %s[%s]",
                out, arr, index
        );
    }
}