package min_camel.ir.Instructions;

import javax.annotation.Nonnull;

public interface Instr  {

    public static enum Type {
        ADD_I,  // Integer addition
        SUB_I,  // Int substraction
        ADD_F,  // Float addition
        SUB_F,  // Float substraction
        MUL_F,  // Multiplication of two numbers
        DIV_F,  // Division of two numbers
        ASSIGN, // Assignment operation
        ARRAY_NEW, // Create a new array
        ARRAY_PUT, // Add an element to the array
        ARRAY_GET, // Access an element inside the array
        LABEL,  // Label definition
        CLOSURE_MAKE,  // Closure_make method
        CLOSURE_APPLY, // Closure_apply method 
        BRANCH, // Branch (if equal / if less than or equal)
        JUMP,   // Jump instruction (branch with no condition)
        CALL,   // Direct function call
        RETURN  // Return from Function
    }

    @Nonnull
    Type getInstrType();

    @Nonnull
    String toString();
}