package min_camel.ir.Instructions;

import min_camel.ir.opr.*;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class CallFunc implements Instr {
    @CheckForNull
    public final Var ret;

    @Nonnull
    public final String name;

    @Nonnull
    public final List<Operand> args;

    public CallFunc(@Nullable Var ret, String s, List<Operand> l) {
        this.ret = ret;
        name = s;
        args = Collections.unmodifiableList(l);
    }

    @Nonnull
    public Type getInstrType() {
        return Type.CALL;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%sCALL %s%s",
                ((ret != null) ? (ret.name + " := ") : ""),
                name, args);
    }

}
