package min_camel.ir.Instructions;

import min_camel.ir.opr.*;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class DivF implements Instr {
    @Nonnull
    public final Var variable;

    @Nonnull
    public final Operand opr1, opr2;

    public DivF(Var v, Operand operand1, Operand operand2) {
        variable = v;
        opr1 = operand1;
        opr2 = operand2;
    }

    @Nonnull
    @Override
    public Type getInstrType() {
        return Type.DIV_F;
    }

    @Nonnull
    @Override
    public String toString() {
        return String.format(
                "%s := %s /. %s",
                variable.name, opr1.toString(), opr2.toString()
        );
    }

}