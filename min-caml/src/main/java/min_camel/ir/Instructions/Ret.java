package min_camel.ir.Instructions;

import min_camel.ir.opr.Operand;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.CheckForNull;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class Ret implements Instr {

    @CheckForNull
    public final Operand opr;

    public Ret(@Nullable Operand o) {
        this.opr = o;
    }

    @Nonnull
    public Type getInstrType() {
        return Type.RETURN;
    }

    @Nonnull
    public String toString() {
        return opr == null ? "RET" : ("RET " + opr);
    }
}
