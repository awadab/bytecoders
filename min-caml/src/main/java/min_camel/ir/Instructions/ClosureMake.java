package min_camel.ir.Instructions;

import min_camel.ir.opr.*;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class ClosureMake implements Instr{
    @Nonnull
    public final Var variable;

    @Nonnull
    public final Label fun;

    @Nonnull
    public final List<Operand> free_args;

    public ClosureMake(Var var, Label l, List<Operand> list){
        this.variable = var;
        fun = l;
        free_args = Collections.unmodifiableList(list);
    }

    @Nonnull
    public Instr.Type getInstrType() {
        return Instr.Type.CLOSURE_MAKE;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%s := ClosureMake(%s,%s)",
                variable.name, fun.name, free_args.toString()
        );
    }
}
