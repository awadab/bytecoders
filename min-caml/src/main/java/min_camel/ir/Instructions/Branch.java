package min_camel.ir.Instructions;

import min_camel.ir.opr.Operand;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class Branch implements Instr {

    public final boolean lessOrEqual;

    @Nonnull
    public final Operand opr1, opr2;

    @Nonnull
    public final Label if_True, if_False;

    public Branch(
            boolean lessOrEqual,
            Operand opr1,
            Operand opr2,
            Label if_True,
            Label if_False
    ) {
        this.lessOrEqual = lessOrEqual;
        this.opr1 = opr1;
        this.opr2 = opr2;
        this.if_True = if_True;
        this.if_False = if_False;
    }

    @Nonnull
    public Type getInstrType() {
        return Type.BRANCH;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "IF (%s %s %s) THEN %s ELSE %s",
                opr1, (lessOrEqual ? "<=" : "="), opr2,
                if_True.name, if_False.name
        );
    }
}
