package min_camel.ir.Instructions;


import min_camel.ir.opr.*;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class ArrNew implements Instr {
    @Nonnull
    public final Var variable;

    @Nonnull
    public final Operand size;

    @CheckForNull
    public final Operand initialize;

    public ArrNew(Var var, Operand size, @Nullable Operand init) {
        this.variable = var;
        this.size = size;
        this.initialize = init;
    }

    @Nonnull
    public Type getInstrType() {
        return Type.ARRAY_NEW;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%s := new Array(%s%s)",
                variable.name, size.toString(),
                initialize == null ? "" : (", " + initialize.toString())
        );
    }
}