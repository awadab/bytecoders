package min_camel.ir.Instructions;

import min_camel.ir.opr.*;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class SubF implements Instr {
    @Nonnull
    public final Var variable;

    @Nonnull
    public final Operand opr1, opr2;

    public SubF(Var v, Operand o1, Operand o2) {
        variable = v;
        opr1 = o1;
        opr2 = o2;
    }

    @Nonnull
    public Type getInstrType() {
        return Type.SUB_F;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%s := %s -. %s",
                variable.name, opr1.toString(), opr2.toString()
        );
    }

}
