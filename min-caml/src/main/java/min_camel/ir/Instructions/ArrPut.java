package min_camel.ir.Instructions;

import min_camel.ir.opr.*;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class ArrPut implements Instr {
    @Nonnull
    public final Var arr;

    @Nonnull
    public final Operand index, value;

    public ArrPut(Var array, Operand index, Operand value) {
        this.arr = array;
        this.index = index;
        this.value = value;
    }

    @Nonnull
    public Type getInstrType() {
        return Type.ARRAY_PUT;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%s[%s] := %s",
                arr, index, value
        );
    }
}
