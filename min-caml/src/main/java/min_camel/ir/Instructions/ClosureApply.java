package min_camel.ir.Instructions;

import min_camel.ir.opr.*;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class ClosureApply implements Instr{
    @CheckForNull
    public final Var ret, closure;

    @Nonnull
    public final List<Operand> args;

    public ClosureApply(@Nullable Var ret, Var cls, List<Operand> l){
        this.ret = ret;
        this.closure = cls;
        args = Collections.unmodifiableList(l);
    }

    @Nonnull
    public Instr.Type getInstrType() {
        return Type.CLOSURE_APPLY;
    }

    @Nonnull
    public String toString() {
        return String.format(
                "%sClosureApply(%s,%s)",
                ((ret != null) ? (ret.name + " := ") : ""),
                closure, args
        );
    }
}