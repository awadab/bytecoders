package min_camel.ir.Instructions;

import min_camel.ir.opr.*;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

@Immutable
public final class Assign implements Instr {
    @Nonnull
    public final Var variable;

    @Nonnull
    public final Operand opr;

    public Assign(Var var, Operand o) {
        this.variable = var;
        this.opr = o;
    }

    @Nonnull
    public Type getInstrType() {
        return Type.ASSIGN;
    }

    @Nonnull
    public String toString() {
        return variable + " := " + opr;
    }


}