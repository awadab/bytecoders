(* giving 3 arguments instead of 2 *)
let rec make_adder x =
  let rec adder y = x + y in
  adder in
print_int (((make_adder 3) 7)9)
