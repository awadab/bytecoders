package min_caml.type;

import org.junit.Test;

import min_camel.ast.AstExp;
import min_camel.comp.Compiler;
import min_camel.helpers.Pair;
import min_camel.type.Checker;
import min_camel.type.Type;
import min_caml.tests.TestHelper;
import min_caml.type.*;
import min_camel.helpers.SymDef;

import javax.annotation.Nonnull;
import java.util.Map;

import static org.junit.Assert.*;

public class CheckerTest extends TestHelper {

    @SuppressWarnings("unused")
    private Checker debugTypes(AstExp p) {
        Checker c = new Checker(p, Compiler.PREDEFS);

        System.out.println("Input:");
        System.out.println("\t" + c.getProgram());
        System.out.println("Equations:");
        for (Pair<Type, Type> eq : c.getEquations()) {
            System.out.println("\t" + eq.left + " = " + eq.right);
        }
        System.out.println("Solution:");
        for (Map.Entry<String, Type> e : c.getSolution().entrySet()) {
            System.out.println("\t" + e.getKey() + " = " + e.getValue());
        }
        System.err.println("Conflicts:");
        for (Pair<Type, Type> e : c.getErrors()) {
            System.out.println("\t" + e.left + " = " + e.right);
        }

        return c;
    }

    private boolean wellTyped(@Nonnull AstExp p) {
        return new Checker(p, Compiler.PREDEFS).wellTyped();
    }

    @Test
    public void testAllSamples() {
        // All the samples must be well-typed
        for (Map.Entry<String, AstExp> sample : all_sampl().entrySet()) {
            assertTrue(sample.getKey(), wellTyped(sample.getValue()));
        }
    }
    
    @Test
    public void test1() {
        // Not well-typed, because the type of `y` can't be determined.
        assertFalse(wellTyped(parse(
                "let rec id x y = x in print_int (id 1 id)"
        )));
    }
    
    @Test
    public void test2() {
        // invalid given test
        assertFalse(wellTyped(parse(
                "print_int()"
        )));
    }
    @Test
    public void test3() {
        // invalid given test
        assertFalse(wellTyped(parse(
                "3"
        )));
    }
    

}