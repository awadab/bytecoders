package min_caml.tests;

import javax.annotation.Nonnull;

import min_camel.ast.AstExp;
import min_camel.comp.Compiler;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.*;
import min_camel.helpers.SymDef;


public abstract class TestHelper {

  

    @Nonnull
    public static AstExp parse(@Nonnull Reader r) {
        try {
            Compiler comp = new Compiler(r);
            if (!comp.parseCode()) {
                comp.printErrors(System.err);
                throw new RuntimeException("Could not parse sample test");
            }
            return comp.getParseTree();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Nonnull
    public static AstExp parse(@Nonnull String s) {
        return parse(new StringReader(s));
    }

    /**
     * A list of sample files that have been included as resources in the
     * `min_caml.tests` package. Names in this list don't include the
     * file extension (.ml).
     */
    private static final String[] samples = {
            "ack", "adder", "cls-bug", "cls-bug2", "cls-rec", "comm",
            "even-odd", "fib", "float", "funcomp", "gcd", "inprod-loop",
            "inprod-rec", "inprod", "join-reg", "join-reg2", "join-stack",
            "join-stack2", "join-stack3", "matmul-flat", "matmul",
            "non-tail-if", "non-tail-if2", "print", "shuffle", "spill",
            "spill2", "spill3", "sum-tail", "sum"
    };

    /**
     * Caches the result of the {@link #all_sampl()} method.
     */
    private static Map<String, AstExp> Samples_parsed;

    @Nonnull
    public static Map<String, AstExp> all_sampl() {
        if (Samples_parsed != null) return Samples_parsed;
        Samples_parsed = new LinkedHashMap<>();
        for (String s : samples) {
            Samples_parsed.put(s, parse(new InputStreamReader(
                    TestHelper.class.getResourceAsStream(s + ".ml")
            )));
        }
        Samples_parsed = Collections.unmodifiableMap(Samples_parsed);
        return Samples_parsed;
    }

}
