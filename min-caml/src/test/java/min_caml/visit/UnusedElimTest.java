package min_caml.visit;

import min_camel.ast.*;
import min_camel.visit.DummyVisitor;
import min_camel.visit.UnusedElim;
import min_caml.tests.TestHelper;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Set;

public class UnusedElimTest extends TestHelper {

    private static void teston(String s) throws Exception {
        AstExp result = UnusedElim.Execute(parse(s));
        checkCorrectness(result);
    }

    @Test
    public void test1() throws Exception {
        teston("let x = 2 in x");
    }

    @Test
    public void test2() throws Exception {
        teston("let x = y in z");
    }

   /* @Test
    public void test3() throws Exception {
        teston("let x = 2 in (let x = 2 in (x + x))");
    }*/

    @Test
    public void test4() throws Exception {
        teston("let x = y in x");
    }


    @Test
    public void test5() throws Exception {
        teston("let rec f x = f x in f");
    }

    @Test
    public void test6() throws Exception {
        teston("let rec fact x = if (x <= 2.0) " +
                        "then 2.0 else (x *. (fact (x -. 2.0))) in fact 7.0"
        );
    }


    @Test
    public void test7() throws Exception {
        teston("let x = x + 4 - z in z = x"
        );
    }

    @Test
    public void test8() throws Exception {
        teston("let rec f x y = x *. y in 5."
        );
    }

    @Test
    public void test9() throws Exception {
        teston("let rec sum x =" +
                        "  if (x <= 0) then 0 else " +
                        "((sum (x - 2)) + x) in sum 20000"
        );
    }

    @Test
    public void test20() throws Exception {
        teston("let rec gcd m n =" +
                        "            if (m = 0) then n else(" +
                        "            if (m <= n) then (gcd m (n - m)) else(" +
                        "    (gcd n (m - n)))) in" +
                        "    gcd 22600 337500"
        );
    }

    @Test
    public void test21() throws Exception {
        teston("let x = 2.23 in let x = 7 in 2"
        );
    }

    @Test
    public void test22() throws Exception {
        teston("let x = 2 in let y = 2 in let z = 3 in let t = 2 in z + e"
        );
    }

    @Test
    public void test23() throws Exception {
        teston("let x = 2 in print_newline()");
    }

/*
    @Test
    public void test24() throws Exception {
        teston("let x = let y = 3 in let z = 4 in let t = z + 2 in 4 + t");
    }*/

    @Test
    public void test25() throws Exception {
        teston("let x = let z = 2 in z + 2 in x + 2");
    }

    private static void checkCorrectness(AstExp exp) {
        exp.accept(new DummyVisitor() {
            private Set<String> bound = new HashSet<>();

            private void bind(AstExp parentExp, String id) {
                if (bound.add(id)) return;
                throw new RuntimeException(id + " already bound in:\n" +
                        parentExp.toString()
                );
            }

            private void unbind(String id) {
                bound.remove(id);
            }

            @Override
            public void visit(@Nonnull AstLet e) {
                String id = e.decl.id;
                bind(e, id);
                super.visit(e);
                unbind(id);
            }

            @Override
            public void visit(@Nonnull AstLetRec e) {
                String id = e.fd.decl.id;
                super.visit(e);
                unbind(id);
            }
        });
    }
}