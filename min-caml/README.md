# Bytecoders
=============
Working in a compiler project using java

## Min-Camel:
==============

# Objective:
This is a group project (as part of the MoSIG graduate program) in which we must create a compiler for the min-caml programming language.

# Build and run the code:
You must have Java and Apache Maven installed on your PC in order to build and run this project. Make sure the commands 'java' and 'mvn' are in your PATH. You'll also need an Internet connection and about 50 MB of disk space the first time you develop the project (so that Maven can do its job).
Simply run 'mvn package' to compile our code. This downloads all dependencies generates the parser runs the unit tests (for typechecking, alpha conversion and beta reduction) and then compiles the project. If everything goes well, a file called 'target/min-camel.jar' will be produced in the new generated folder "target".
Use 'mvn test' if you simply want to run the unit tests.
After compiling the project, run 'java -jar target/min-camel.jar', followed by any command-line options that are particular to our application. You can view a quick summary of the various commands by launching the application without any command-line inputs. 'java -jar target/min-camel.jar -v', for example, will display the current version number of our application.

For example you can run a simple test for type checking:
java -jar target/min-camel.jar -t src/test/resources/min_caml/typechecking/invalid/call_bad_arg.ml
If any issue occurs the corresponding error message will be displayed in the terminal.

## Using our program

The application will read source code written in the min-caml language (examples can be found in 'src/test/resources/min_camel/tests') and generate ARM assembly code (but not machine code). To create an ARM executable, you must additionally feed this result to an assembler.

For example, given a source file `print.ml` (written in min-caml), you should
execute these commands in order to produce an ARM executable file:

- `java -jar target/min-camel.jar print.ml -o print.s`
- `arm-none-eabi-as -o print.o   print.s ARM/libmincaml.S`
- `arm-none-eabi-ld -o printexecutable print.o`
- `qemu-arm ./printexecutable` (to run the program in an emulator)

## Navigating the project files

Provided files and folders:

- `src/main/cup/Parser.cup` - Grammar of the *min-caml* language.
- `src/main/flex/Lexer.flex` - The scanner/tokenizer for parser.
- `src/main/java/*` - Java source code.
- `src/test/java/*` - Java unit testing code.
- `src/test/resources/min_camel/tests/*.ml` - Sample source codes written
        in the *min-caml* language provided by intructors.

The following are generated from `Parser.cup` and `Lexer.flex` by running
the appropriate Maven tasks, and become part of the compilation process:

- `target/generated-sources/cup` - Java sources of the generated parser.
- `target/generated-sources/flex` - Java sources of the generated lexer.

## Licensing:
=============
You are free to use, alter, and/or distribute the code for business or non-commercial reasons without having to give credit. This project's data and source code are all licensed under the Public Domain, which means there are no warranties or copyright restrictions imposed by us.